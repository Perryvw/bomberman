﻿package  {
	
	import flash.display.MovieClip;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	public class ScoreBoard extends MovieClip {
		
		var userScores:Array;
		var userOrder:Array;
		var numUsers:int;
		
		public function ScoreBoard() {
			userScores = new Array();
			userOrder = new Array();
			this.visible = false;
		}
		
		public function addPlayer(playerName:String, slot:int) {
			var us:UserScore = new UserScore(playerName, slot);
			this.addChild(us);
			us.x = 250;
			us.y = numUsers*39;
			us.resetScore();
			userScores[slot] = us;
			userOrder[numUsers] = us;
			
			numUsers++;
			TweenLite.to(us, 1, {x:0, ease:Bounce.easeOut});
		}
		
		public function pointScored( slot:int ) {
			userScores[slot].incrementScore( function() {
				var currentScore:int = userScores[slot].score;
				var currentSlot:int = 9;
				
				for (var i=0;i<userOrder.length;i++){
					if (userOrder[i] == userScores[slot]){
						currentSlot = i;
						break;
					}
				}
				
				var nextSlot:int = 0;
				for (i=userOrder.length-1;i>=0;i--){
					if (userOrder[i].score > currentScore){
						nextSlot = i+1;
						break;
					}
				}
							
				for (i=userOrder.length-1;i>=0;i--)
				{
					if (i < currentSlot && i>= nextSlot){
						userOrder[i+1] = userOrder[i];
						TweenLite.to(userOrder[i], 1, {y:39*(i+1), ease:Quart.easeInOut});
					}
				}
				
				userOrder[nextSlot] = userScores[slot];
				TweenLite.to(userOrder[nextSlot], 1, {y:39*(nextSlot), ease:Quart.easeInOut, onComplete: function(){
					 for (var j:int=0;j<userScores.length;j++){
						 userScores[j].userNameTxt.textColor = 0xffffff;
					 }
					 userOrder[0].userNameTxt.textColor = 0xffeb0d;
				}});
			});			
		}
		
		public function screenResize(screenW:int, screenH:int, hScale:Number, vScale:Number){
			this.scaleX = hScale;
			this.scaleY = vScale;
			
			this.x = screenW - 1;
			this.y = 150*vScale;
		}
	}	
}
