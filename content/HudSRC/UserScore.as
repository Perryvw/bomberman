﻿package  {
	
	import flash.display.MovieClip;	
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	public class UserScore extends MovieClip {
		
		var colorCodes:Array = new Array(
			0x3375ff,
			0x66ffbf,
			0xbf00bf,
			0xf3f00b,
			0xff6b00,
			0xfe86c2
		);
		
		public var score:int;
		
		public function UserScore(userName:String, slot:int) {
			this.graphics.lineStyle(1, this.colorCodes[slot]);
			this.graphics.drawRect(-249, 0, 248, 36);
			this.userNameTxt.text = userName;
			this.score = 0;
		}
		
		public function incrementScore(callback) {
			if (this.score == 10) return;
			this.score += 1;
			var stripe:MovieClip = this["sc"+this.score];
			stripe.alpha = 0;
			stripe.scaleX=3;
			stripe.scaleY=3;
			TweenLite.to(stripe, 0.3, {scaleX:1, scaleY:1, alpha:1, delay:1, onComplete:callback});
			this["sc"+this.score].visible = true;
		}
		
		public function resetScore() {
			for (var i:int=1;i<=10;i++) {
				this["sc"+i].visible = false;
			}
			this.score = 0;
		}
	}
	
}
