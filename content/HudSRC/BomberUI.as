﻿package {
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import flash.net.*;
	import flash.events.Event;
	
	import dota2Net.D2HTTPSocket;

	//import some stuff from the valve lib
	import ValveLib.Globals;
	import ValveLib.ResizeManager;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import scaleform.gfx.InteractiveObjectEx;
	
	public class BomberUI extends MovieClip{
		
		//these three variables are required by the engine
		public var gameAPI:Object;
		public var globals:Object;
		public var elementName:String;
		
		private var screenWidth:int = 0;
		
		private var logParts:Array;
		private var logPartNum:int;
		private var partsReceived:int;
		
		//constructor, you usually will use onLoaded() instead
		public function BomberUI() : void {
		}
						
		//this function is called when the UI is loaded
		public function onLoaded() : void {			
			//make this UI visible
			visible = true;
			
			//let the client rescale the UI
			Globals.instance.resizeManager.AddListener(this);
			
			//listen to events
			this.gameAPI.SubscribeToGameEvent("bomber_player_bombs_update", this.onBombUpdate);
			this.gameAPI.SubscribeToGameEvent("bomber_bombs_reset", this.onBombReset);
			this.gameAPI.SubscribeToGameEvent("bomber_start_game", this.onGameStart);
			this.gameAPI.SubscribeToGameEvent("bomber_player_join", this.addPlayer);
			this.gameAPI.SubscribeToGameEvent("bomber_point_scored", this.addPointToPlayer);
			this.gameAPI.SubscribeToGameEvent("bomber_player_powerup", this.getPowerUp);
			
			this.gameAPI.SubscribeToGameEvent("bomber_send_log_init", this.sendLogInit);
			this.gameAPI.SubscribeToGameEvent("bomber_send_log_chunk", this.sendLogPart);
			
			scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.bombCounter, true);
			
			//this is not needed, but it shows you your UI has loaded (needs 'scaleform_spew 1' in console)
			trace("Custom UI loaded!");
		}
		
		public function onResize(re:ResizeManager) : * {
			
			// calculate by what ratio the stage is scaling
			var scaleRatioX:Number = re.ScreenWidth/1600;
			var scaleRatioY:Number = re.ScreenHeight/900;
			
			if (re.ScreenWidth > 1600){
				scaleRatioX = 1;
			}
			if (re.ScreenHeight > 900){
				scaleRatioY = 1;
			}
			
			screenWidth = re.ScreenWidth;
			
			//pass the resize event to our module, we pass the width and height of the screen, as well as the INVERSE of the stage scaling ratios.
			this.bombCounter.screenResize(re.ScreenWidth, re.ScreenHeight, scaleRatioY, scaleRatioY, re.IsWidescreen());
			this.scoreboard.screenResize(re.ScreenWidth, re.ScreenHeight, scaleRatioY, scaleRatioY);
		}
		
		private function sendLogInit( args:Object ){
			logParts = new Array();
			logPartNum = args.numChunks;
			partsReceived = 0;
		}
		
		private function sendLogPart( args:Object ){
			logParts[args.id] = args.data;
			partsReceived++;
			
			if (logPartNum == partsReceived) {
				sendLog( logParts )
			}
		}
		
		private function sendLog( parts:Array ) {
			var socket:D2HTTPSocket = new D2HTTPSocket('yrrep.me', '185.13.226.226');
			var data = parts.join('');
			socket.postDataAsync('dota/bomberman/logs.php', '{"userID" : '+globals.Players.GetLocalPlayer()+', "log" : '+data+'}', socketReturn);
		}
		
		private function socketReturn( statusCode:int, data:String ) {
			trace('Response status: '+statusCode);
			trace(data);
		}
		
		private function onBombUpdate( args:Object ) {
			if (globals.Players.GetLocalPlayer() == args.playerID) {
				bombCounter.updateBombs(args.total, args.available);
			}
		}
		
		private function onBombReset( args:Object ) {
			bombCounter.reset();
		}
		
		private function onGameStart( args:Object ) {
			bombCounter.visible = true;
			scoreboard.visible = true;
			this.gameAPI.SendServerCommand("PlayerName "+globals.Players.GetPlayerName(globals.Players.GetLocalPlayer()));
		}
		
		private function addPlayer( args:Object ) {
			this.scoreboard.addPlayer(globals.Players.GetPlayerName(args.slot), args.slot);
		}
		
		private function addPointToPlayer( args:Object ) {
			this.scoreboard.pointScored( args.ID );
		}
		
		private function getPowerUp( args:Object ) {
			if (args.playerID != globals.Players.GetLocalPlayer()) return;
			
			var heroEntIndex:int = globals.Players.GetPlayerHeroEntityIndex( args.playerID );
			var playerLoc:Object = globals.Entities.GetAbsOrigin( heroEntIndex );
			
			trace(playerLoc);
			trace(playerLoc[0]+", "+playerLoc[1]+", "+playerLoc[2]);
			var popup:PowerUpPopup = new PowerUpPopup();
			scaleform.gfx.InteractiveObjectEx.setHitTestDisable(popup, true);
			
			this.addChild(popup);
			
			if (args.powerUpType == 3){
				popup.textBox.text = '#tt_speed_up';
				popup.textBox.textColor = 0xff0000;
			} else if (args.powerUpType == 1){
				popup.textBox.text = '#tt_power_up';
				popup.textBox.textColor = 0x0000ff;
			} else if (args.powerUpType == 2){
				popup.textBox.text = '#tt_bombs_up';
				popup.textBox.textColor = 0xffff00;
			}else {
				popup.textBox.text = '#tt_kick_up';
				popup.textBox.textColor = 0x00ff00;
			}
			
			popup.x = globals.Game.WorldToScreenX(playerLoc[0], playerLoc[1], playerLoc[2] + 250);
			popup.y = globals.Game.WorldToScreenY(playerLoc[0], playerLoc[1], playerLoc[2] + 250);
			
			TweenLite.to(popup, 1.3, {alpha:0, y:(popup.y-30), onComplete:function() { popup.x= 9999; popup.width=0; stage.removeChild(popup); }})
			
			if (args.powerUpType == 4 && !this.bombCounter.kickBomb.visible) {
				this.bombCounter.kickBomb.scaleX = 0;
				this.bombCounter.kickBomb.scaleY = 0;
				this.bombCounter.kickBomb.visible = true;
				TweenLite.to(this.bombCounter.kickBomb, 0.8, {scaleX:1,scaleY:1,ease:Elastic.easeOut});
			}
		}
	}
}