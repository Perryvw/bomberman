﻿package  {
	
	import flash.display.MovieClip;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	public class BombCounter extends MovieClip {
		
		var totalBombs = 1;
		
		public function BombCounter() {
			for (var i:int=2;i<=10;i++){
				this["b"+i].visible = false;			
			}
			kickBomb.visible = false;
			this.visible = false;
		}
		
		public function screenResize(stageW:int, stageH:int, xScale:Number, yScale:Number, wide:Boolean){
			this.x = stageW/2+40;
			this.y = stageH-175*yScale;
			 
			//Now we just set the scale of this element, because these parameters are already the inverse ratios
			this.scaleX = xScale;
			this.scaleY = yScale;
		}
		
		public function updateBombs( total:int, available:int ) {
			if (total > totalBombs) {
				this["b1"].scaleX = 0;
				this["b1"].scaleY = 0;
				this["b"+total].visible = true;
				TweenLite.to(this["b1"], 0.8, {scaleX:1,scaleY:1,ease:Elastic.easeOut});
				totalBombs = total;
			}
			
			for (var i:int = 1; i<= total;i++){
				if (i <= available) {
					this["b"+i].alpha = 1.0;
				}
				else {
					this["b"+i].alpha = 0.5;
				}
			}			
		}
		
		public function reset() {
			totalBombs = 1;
			for (var i:int=2;i<=10;i++){
				this["b"+i].visible = false;			
			}
			this.kickBomb.visible = false;
		}
	}	
}
