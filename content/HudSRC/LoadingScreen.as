﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;
	
	import flash.display.*;
	import flash.events.*;
	
	import ValveLib.Globals;
	import ValveLib.ResizeManager;
	
	public class LoadingScreen extends MovieClip
	{
		// element details filled out by game engine
		public var gameAPI:Object;
		public var globals:Object;
		public var elementName:String;
		
		public function LoadingScreen() {
		}
		
		//this function is called when the UI is loaded
		public function onLoaded() : void {			
			//make this UI visible
			visible = true;
			
			//let the client rescale the UI
			Globals.instance.resizeManager.AddListener(this);
						
			//listen to events
			this.gameAPI.SubscribeToGameEvent("bomber_start_game", this.onGameStart);
			
			//this is not needed, but it shows you your UI has loaded (needs 'scaleform_spew 1' in console)
			trace("Showing loading screen.");
		}
		
		private function onGameStart( args:Object ) {
			this.removeChild(this.lscreen);
			this.removeChild(this.infoMc);
		}
		
		//this handles the resizes - credits to SinZ
		public function onResize(re:ResizeManager) : * {
			
			this.lscreen.scaleX = re.ScreenWidth/1600;
			this.lscreen.scaleY = re.ScreenHeight/900;
						
			
			//pass the resize event to our module, we pass the width and height of the screen, as well as the INVERSE of the stage scaling ratios.
			this.lscreen.x = stage.stageWidth/2;
			this.lscreen.y = stage.stageHeight/2;
			//this.lscreen.resizeBy(scaleRatioX, scaleRatioY);
			
			this.infoMc.x = 20;
			this.infoMc.y = 30;
		}
	}
}
