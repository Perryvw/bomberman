// Dota Units File
"DOTAUnits"
{
	"Version"	"1"	

	"bomber_bomb"							
	{										
		// General
		//----------------------------------------------------------------
		"Model"						"models/heroes/techies/fx_techies_remotebomb.vmdl"	// Model.
		"BaseClass"					"npc_dota_creature"
		"SoundSet"					"0"			// Name of sound set.
		"Level"						"1"
		"ModelScale"				"1.2"

		// Abilities
		//----------------------------------------------------------------
		"Ability1"					"bomb_passive"			// Ability 1
		"Ability2"					""			// Ability 2
		"Ability3"					""			// Ability 3
		"Ability4"					""			// Ability 4

		// Armor
		//----------------------------------------------------------------
		"ArmorPhysical"				"0"			// Physical protection.

		// Attack
		//----------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_NO_ATTACK"
		"AttackDamageMin"			"0"			// Damage range min.
		"AttackDamageMax"			"0"			// Damage range max.	
		"AttackRate"				"1.0"		// Speed of attack.
		"AttackAnimationPoint"		"0.0"		// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"0"			// Range within a target can be acquired.
		"AttackRange"				"0"			// Range within a target can be attacked.
		"ProjectileModel"			""			// Particle system model for projectile.
		"ProjectileSpeed"			"0"			// Speed of projectile.		   

		// Bounty
		//----------------------------------------------------------------
		"BountyXP"					"0"		// Experience earn.
		"BountyGoldMin"				"0"		// Gold earned min.
		"BountyGoldMax"				"0"		// Gold earned max.

		// Movement
		//----------------------------------------------------------------
		"MovementCapabilities"		"DOTA_UNIT_CAP_MOVE_GROUND"
		"MovementSpeed"				"522"			// Speed.
		"MovementTurnRate"			"4.0"		// Turning rate.

		// Status
		//----------------------------------------------------------------
		"StatusHealth"				"100"		// Base health.
		"StatusHealthRegen"			"0.0"		// Health regeneration rate.
		
		// Vision
		//----------------------------------------------------------------
		"VisionDaytimeRange"		"400"		// Range of vision during day light.
		"VisionNighttimeRange"		"400"		// Range of vision at night time.

		// Team
		//----------------------------------------------------------------
		"TeamName"					"DOTA_TEAM_NEUTRALS"							// Team name.
		"CombatClassAttack"			"DOTA_COMBAT_CLASS_ATTACK_BASIC"
		"CombatClassDefend"			"DOTA_COMBAT_CLASS_DEFEND_BASIC"
		"UnitRelationshipClass"		"DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"
	}
	
	"bomber_barricade"							
	{										
		// General
		//----------------------------------------------------------------
		"Model"						"models\props_debris\shop_set_cage001.vmdl"	// Model.
		"BaseClass"					"npc_dota_creature"
		"SoundSet"					"0"			// Name of sound set.
		"Level"						"1"
		"ModelScale"				"1.4"

		// Abilities
		//----------------------------------------------------------------
		"Ability1"					"barricade_passive"			// Ability 1
		"Ability2"					""			// Ability 2
		"Ability3"					""			// Ability 3
		"Ability4"					""			// Ability 4

		// Armor
		//----------------------------------------------------------------
		"ArmorPhysical"				"0"			// Physical protection.

		// Attack
		//----------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_NO_ATTACK"
		"AttackDamageMin"			"0"			// Damage range min.
		"AttackDamageMax"			"0"			// Damage range max.	
		"AttackRate"				"1.0"		// Speed of attack.
		"AttackAnimationPoint"		"0.0"		// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"0"			// Range within a target can be acquired.
		"AttackRange"				"0"			// Range within a target can be attacked.
		"ProjectileModel"			""			// Particle system model for projectile.
		"ProjectileSpeed"			"0"			// Speed of projectile.		   

		// Bounty
		//----------------------------------------------------------------
		"BountyXP"					"0"		// Experience earn.
		"BountyGoldMin"				"0"		// Gold earned min.
		"BountyGoldMax"				"0"		// Gold earned max.

		// Movement
		//----------------------------------------------------------------
		"MovementCapabilities"		"DOTA_UNIT_CAP_MOVE_NONE"

		// Status
		//----------------------------------------------------------------
		"StatusHealth"				"100"		// Base health.
		"StatusHealthRegen"			"0.0"		// Health regeneration rate.
		
		"RingRadius"				"0"
		
		// Vision
		//----------------------------------------------------------------
		"VisionDaytimeRange"		"400"		// Range of vision during day light.
		"VisionNighttimeRange"		"400"		// Range of vision at night time.

		// Team
		//----------------------------------------------------------------
		"TeamName"					"DOTA_TEAM_NEUTRALS"							// Team name.
		"CombatClassAttack"			"DOTA_COMBAT_CLASS_ATTACK_BASIC"
		"CombatClassDefend"			"DOTA_COMBAT_CLASS_DEFEND_BASIC"
		"UnitRelationshipClass"		"DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"
	}

	"bomber_barricade_invul"							
	{										
		// General
		//----------------------------------------------------------------
		"Model"						"models/props_rock/riveredge_rock008a.vmdl"	// Model.
		"BaseClass"					"npc_dota_creature"
		"SoundSet"					"0"			// Name of sound set.
		"Level"						"1"
		"ModelScale"				"1"

		// Abilities
		//----------------------------------------------------------------
		"Ability1"					"barricade_passive"			// Ability 1
		"Ability2"					""			// Ability 2
		"Ability3"					""			// Ability 3
		"Ability4"					""			// Ability 4

		// Armor
		//----------------------------------------------------------------
		"ArmorPhysical"				"0"			// Physical protection.

		// Attack
		//----------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_NO_ATTACK"
		"AttackDamageMin"			"0"			// Damage range min.
		"AttackDamageMax"			"0"			// Damage range max.	
		"AttackRate"				"1.0"		// Speed of attack.
		"AttackAnimationPoint"		"0.0"		// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"0"			// Range within a target can be acquired.
		"AttackRange"				"0"			// Range within a target can be attacked.
		"ProjectileModel"			""			// Particle system model for projectile.
		"ProjectileSpeed"			"0"			// Speed of projectile.		   

		// Bounty
		//----------------------------------------------------------------
		"BountyXP"					"0"		// Experience earn.
		"BountyGoldMin"				"0"		// Gold earned min.
		"BountyGoldMax"				"0"		// Gold earned max.

		// Movement
		//----------------------------------------------------------------
		"MovementCapabilities"		"DOTA_UNIT_CAP_MOVE_NONE"

		// Status
		//----------------------------------------------------------------
		"StatusHealth"				"100"		// Base health.
		"StatusHealthRegen"			"0.0"		// Health regeneration rate.
		
		"RingRadius"				"0"
		
		// Vision
		//----------------------------------------------------------------
		"VisionDaytimeRange"		"400"		// Range of vision during day light.
		"VisionNighttimeRange"		"400"		// Range of vision at night time.

		// Team
		//----------------------------------------------------------------
		"TeamName"					"DOTA_TEAM_NEUTRALS"							// Team name.
		"CombatClassAttack"			"DOTA_COMBAT_CLASS_ATTACK_BASIC"
		"CombatClassDefend"			"DOTA_COMBAT_CLASS_DEFEND_BASIC"
		"UnitRelationshipClass"		"DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"
	}
	
	"bomber_haste_rune"							
	{										
		// General
		//----------------------------------------------------------------
		"Model"						"models/props_gameplay/rune_haste01.mdl"	// Model.
		
		"BaseClass"					"npc_dota_creature"
		"SoundSet"					"0"			// Name of sound set.
		"Level"						"1"
		"ModelScale"				"1.2"

		// Abilities
		//----------------------------------------------------------------
		"Ability1"					"rune_modifier_haste"			// Ability 1
		"Ability2"					""			// Ability 2
		"Ability3"					""			// Ability 3
		"Ability4"					""			// Ability 4

		// Armor
		//----------------------------------------------------------------
		"ArmorPhysical"				"0"			// Physical protection.

		// Attack
		//----------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_NO_ATTACK"
		"AttackDamageMin"			"0"			// Damage range min.
		"AttackDamageMax"			"0"			// Damage range max.	
		"AttackRate"				"1.0"		// Speed of attack.
		"AttackAnimationPoint"		"0.0"		// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"0"			// Range within a target can be acquired.
		"AttackRange"				"0"			// Range within a target can be attacked.
		"ProjectileModel"			""			// Particle system model for projectile.
		"ProjectileSpeed"			"0"			// Speed of projectile.		   

		// Bounty
		//----------------------------------------------------------------
		"BountyXP"					"0"		// Experience earn.
		"BountyGoldMin"				"0"		// Gold earned min.
		"BountyGoldMax"				"0"		// Gold earned max.

		// Movement
		//----------------------------------------------------------------
		"MovementCapabilities"		"DOTA_UNIT_CAP_MOVE_NONE"
		"MovementSpeed"				"522"			// Speed.
		"MovementTurnRate"			"4.0"		// Turning rate.

		// Status
		//----------------------------------------------------------------
		"StatusHealth"				"100"		// Base health.
		"StatusHealthRegen"			"0.0"		// Health regeneration rate.
		
		// Vision
		//----------------------------------------------------------------
		"VisionDaytimeRange"		"400"		// Range of vision during day light.
		"VisionNighttimeRange"		"400"		// Range of vision at night time.

		// Team
		//----------------------------------------------------------------
		"TeamName"					"DOTA_TEAM_NEUTRALS"							// Team name.
		"CombatClassAttack"			"DOTA_COMBAT_CLASS_ATTACK_BASIC"
		"CombatClassDefend"			"DOTA_COMBAT_CLASS_DEFEND_BASIC"
		"UnitRelationshipClass"		"DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"
	}
	
	"bomber_power_rune"							
	{										
		// General
		//----------------------------------------------------------------
		"Model"						"models/props_gameplay/rune_doubledamage01.mdl"	// Model.
		
		"BaseClass"					"npc_dota_creature"
		"SoundSet"					"0"			// Name of sound set.
		"Level"						"1"
		"ModelScale"				"1.2"

		// Abilities
		//----------------------------------------------------------------
		"Ability1"					"rune_modifier_power"			// Ability 1
		"Ability2"					""			// Ability 2
		"Ability3"					""			// Ability 3
		"Ability4"					""			// Ability 4

		// Armor
		//----------------------------------------------------------------
		"ArmorPhysical"				"0"			// Physical protection.

		// Attack
		//----------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_NO_ATTACK"
		"AttackDamageMin"			"0"			// Damage range min.
		"AttackDamageMax"			"0"			// Damage range max.	
		"AttackRate"				"1.0"		// Speed of attack.
		"AttackAnimationPoint"		"0.0"		// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"0"			// Range within a target can be acquired.
		"AttackRange"				"0"			// Range within a target can be attacked.
		"ProjectileModel"			""			// Particle system model for projectile.
		"ProjectileSpeed"			"0"			// Speed of projectile.		   

		// Bounty
		//----------------------------------------------------------------
		"BountyXP"					"0"		// Experience earn.
		"BountyGoldMin"				"0"		// Gold earned min.
		"BountyGoldMax"				"0"		// Gold earned max.

		// Movement
		//----------------------------------------------------------------
		"MovementCapabilities"		"DOTA_UNIT_CAP_MOVE_NONE"
		"MovementSpeed"				"522"			// Speed.
		"MovementTurnRate"			"4.0"		// Turning rate.

		// Status
		//----------------------------------------------------------------
		"StatusHealth"				"100"		// Base health.
		"StatusHealthRegen"			"0.0"		// Health regeneration rate.
		
		// Vision
		//----------------------------------------------------------------
		"VisionDaytimeRange"		"400"		// Range of vision during day light.
		"VisionNighttimeRange"		"400"		// Range of vision at night time.

		// Team
		//----------------------------------------------------------------
		"TeamName"					"DOTA_TEAM_NEUTRALS"							// Team name.
		"CombatClassAttack"			"DOTA_COMBAT_CLASS_ATTACK_BASIC"
		"CombatClassDefend"			"DOTA_COMBAT_CLASS_DEFEND_BASIC"
		"UnitRelationshipClass"		"DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"
	}
	
	"bomber_bomb_rune"							
	{										
		// General
		//----------------------------------------------------------------
		"Model"						"models/props_gameplay/rune_illusion01.mdl"	// Model.
		
		"BaseClass"					"npc_dota_creature"
		"SoundSet"					"0"			// Name of sound set.
		"Level"						"1"
		"ModelScale"				"1.2"

		// Abilities
		//----------------------------------------------------------------
		"Ability1"					"rune_modifier_bomb"			// Ability 1
		"Ability2"					""			// Ability 2
		"Ability3"					""			// Ability 3
		"Ability4"					""			// Ability 4

		// Armor
		//----------------------------------------------------------------
		"ArmorPhysical"				"0"			// Physical protection.

		// Attack
		//----------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_NO_ATTACK"
		"AttackDamageMin"			"0"			// Damage range min.
		"AttackDamageMax"			"0"			// Damage range max.	
		"AttackRate"				"1.0"		// Speed of attack.
		"AttackAnimationPoint"		"0.0"		// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"0"			// Range within a target can be acquired.
		"AttackRange"				"0"			// Range within a target can be attacked.
		"ProjectileModel"			""			// Particle system model for projectile.
		"ProjectileSpeed"			"0"			// Speed of projectile.		   

		// Bounty
		//----------------------------------------------------------------
		"BountyXP"					"0"		// Experience earn.
		"BountyGoldMin"				"0"		// Gold earned min.
		"BountyGoldMax"				"0"		// Gold earned max.

		// Movement
		//----------------------------------------------------------------
		"MovementCapabilities"		"DOTA_UNIT_CAP_MOVE_NONE"
		"MovementSpeed"				"522"			// Speed.
		"MovementTurnRate"			"4.0"		// Turning rate.

		// Status
		//----------------------------------------------------------------
		"StatusHealth"				"100"		// Base health.
		"StatusHealthRegen"			"0.0"		// Health regeneration rate.
		
		// Vision
		//----------------------------------------------------------------
		"VisionDaytimeRange"		"400"		// Range of vision during day light.
		"VisionNighttimeRange"		"400"		// Range of vision at night time.

		// Team
		//----------------------------------------------------------------
		"TeamName"					"DOTA_TEAM_NEUTRALS"							// Team name.
		"CombatClassAttack"			"DOTA_COMBAT_CLASS_ATTACK_BASIC"
		"CombatClassDefend"			"DOTA_COMBAT_CLASS_DEFEND_BASIC"
		"UnitRelationshipClass"		"DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"
	}
	
	"bomber_kick_rune"							
	{										
		// General
		//----------------------------------------------------------------
		"Model"						"models/props_gameplay/rune_regeneration01.mdl"	// Model.
		
		"BaseClass"					"npc_dota_creature"
		"SoundSet"					"0"			// Name of sound set.
		"Level"						"1"
		"ModelScale"				"1.2"

		// Abilities
		//----------------------------------------------------------------
		"Ability1"					"rune_modifier_kick"			// Ability 1
		"Ability2"					""			// Ability 2
		"Ability3"					""			// Ability 3
		"Ability4"					""			// Ability 4

		// Armor
		//----------------------------------------------------------------
		"ArmorPhysical"				"0"			// Physical protection.

		// Attack
		//----------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_NO_ATTACK"
		"AttackDamageMin"			"0"			// Damage range min.
		"AttackDamageMax"			"0"			// Damage range max.	
		"AttackRate"				"1.0"		// Speed of attack.
		"AttackAnimationPoint"		"0.0"		// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"0"			// Range within a target can be acquired.
		"AttackRange"				"0"			// Range within a target can be attacked.
		"ProjectileModel"			""			// Particle system model for projectile.
		"ProjectileSpeed"			"0"			// Speed of projectile.		   

		// Bounty
		//----------------------------------------------------------------
		"BountyXP"					"0"		// Experience earn.
		"BountyGoldMin"				"0"		// Gold earned min.
		"BountyGoldMax"				"0"		// Gold earned max.

		// Movement
		//----------------------------------------------------------------
		"MovementCapabilities"		"DOTA_UNIT_CAP_MOVE_NONE"
		"MovementSpeed"				"522"			// Speed.
		"MovementTurnRate"			"4.0"		// Turning rate.

		// Status
		//----------------------------------------------------------------
		"StatusHealth"				"100"		// Base health.
		"StatusHealthRegen"			"0.0"		// Health regeneration rate.
		
		// Vision
		//----------------------------------------------------------------
		"VisionDaytimeRange"		"400"		// Range of vision during day light.
		"VisionNighttimeRange"		"400"		// Range of vision at night time.

		// Team
		//----------------------------------------------------------------
		"TeamName"					"DOTA_TEAM_NEUTRALS"							// Team name.
		"CombatClassAttack"			"DOTA_COMBAT_CLASS_ATTACK_BASIC"
		"CombatClassDefend"			"DOTA_COMBAT_CLASS_DEFEND_BASIC"
		"UnitRelationshipClass"		"DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"
	}
	
	"bomber_remote_rune"							
	{										
		// General
		//----------------------------------------------------------------
		"Model"						"models/props_gameplay/rune_invisibility01.mdl"	// Model.
		
		"BaseClass"					"npc_dota_creature"
		"SoundSet"					"0"			// Name of sound set.
		"Level"						"1"
		"ModelScale"				"1.2"

		// Abilities
		//----------------------------------------------------------------
		"Ability1"					"rune_modifier_remote"			// Ability 1
		"Ability2"					""			// Ability 2
		"Ability3"					""			// Ability 3
		"Ability4"					""			// Ability 4

		// Armor
		//----------------------------------------------------------------
		"ArmorPhysical"				"0"			// Physical protection.

		// Attack
		//----------------------------------------------------------------
		"AttackCapabilities"		"DOTA_UNIT_CAP_NO_ATTACK"
		"AttackDamageMin"			"0"			// Damage range min.
		"AttackDamageMax"			"0"			// Damage range max.	
		"AttackRate"				"1.0"		// Speed of attack.
		"AttackAnimationPoint"		"0.0"		// Normalized time in animation cycle to attack.
		"AttackAcquisitionRange"	"0"			// Range within a target can be acquired.
		"AttackRange"				"0"			// Range within a target can be attacked.
		"ProjectileModel"			""			// Particle system model for projectile.
		"ProjectileSpeed"			"0"			// Speed of projectile.		   

		// Bounty
		//----------------------------------------------------------------
		"BountyXP"					"0"		// Experience earn.
		"BountyGoldMin"				"0"		// Gold earned min.
		"BountyGoldMax"				"0"		// Gold earned max.

		// Movement
		//----------------------------------------------------------------
		"MovementCapabilities"		"DOTA_UNIT_CAP_MOVE_NONE"
		"MovementSpeed"				"522"			// Speed.
		"MovementTurnRate"			"4.0"		// Turning rate.

		// Status
		//----------------------------------------------------------------
		"StatusHealth"				"100"		// Base health.
		"StatusHealthRegen"			"0.0"		// Health regeneration rate.
		
		// Vision
		//----------------------------------------------------------------
		"VisionDaytimeRange"		"400"		// Range of vision during day light.
		"VisionNighttimeRange"		"400"		// Range of vision at night time.

		// Team
		//----------------------------------------------------------------
		"TeamName"					"DOTA_TEAM_NEUTRALS"							// Team name.
		"CombatClassAttack"			"DOTA_COMBAT_CLASS_ATTACK_BASIC"
		"CombatClassDefend"			"DOTA_COMBAT_CLASS_DEFEND_BASIC"
		"UnitRelationshipClass"		"DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"
	}
}
