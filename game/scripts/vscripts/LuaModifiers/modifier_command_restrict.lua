modifier_command_restrict = class({})

function modifier_command_restrict:CheckState()
	local state = {
		[MODIFIER_STATE_COMMAND_RESTRICTED] = true,
	}
 
	return state
end