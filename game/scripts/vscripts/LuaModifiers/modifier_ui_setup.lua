modifier_ui_setup = class({})

function modifier_ui_setup:IsHidden()
	return false
end

function modifier_ui_setup:OnCreated( params )
	if not IsServer() then
		Convars:SetInt( 'dota_render_bottom_inset', 0 )
	end
end