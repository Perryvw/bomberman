modifier_bomb_speed = class({})

function modifier_bomb_speed:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_MAX,
		MODIFIER_PROPERTY_MOVESPEED_LIMIT,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE_MIN,
	}

	return funcs
end

function modifier_bomb_speed:GetModifierMoveSpeed_Absolute( params )
	return 1000
end

function modifier_bomb_speed:GetModifierMoveSpeed_AbsoluteMin( params )
	return 1000
end

function modifier_bomb_speed:GetModifierMoveSpeed_Max( params )
	return 1000
end

function modifier_bomb_speed:GetModifierMoveSpeed_Limit( params )
	return 1000
end
