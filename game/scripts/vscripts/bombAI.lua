bombAI = class({})

bombAI.AI_THINK_INTERVAL = 0.2

function bombAI:constructor(hero)
	self.entindex = hero:GetEntityIndex()
	self.hero = hero
	self.worldversion = -1
	self.lastBomb = 0

	self.hero.onBombPlace = function()
		self.lastBomb = GameRules:GetGameTime()
	end

	-- Start thinking
	Timers:CreateTimer(function()
		if self.enabled == true then
			Dynamic_Wrap(bombAI, "Think")(self)
		end
		return bombAI.AI_THINK_INTERVAL
	end)
end

function bombAI:Enabled(bool)
	self.enabled = false
	self.bombPos = nil
	self.safeVersion = -1

	ExecuteOrderFromTable({
		UnitIndex = self.entindex,
		OrderType = DOTA_UNIT_ORDER_STOP
	})

	if bool == true then
		Timers:CreateTimer(4, function()
			self.worldversion = -2
			self.enabled = true
		end)
	end
end

function bombAI:Think()
	if not self.hero:IsAlive() then self.dead = true return end
	if self.dead == true then
		self.dead = false
		ExecuteOrderFromTable({
			UnitIndex = self.entindex,
			OrderType = DOTA_UNIT_ORDER_STOP
		})
		self.worldversion = AIWorld.version
	end

	if AIWorld:IsSafe(self.hero:GetAbsOrigin()) == false and self.safeVersion ~= AIWorld.version then
		local path = AIWorld:GetShortestSafetyPath(self.hero:GetAbsOrigin())
		self.safeVersion = AIWorld.version

		ExecuteOrderFromTable({
			UnitIndex = self.entindex,
			OrderType = DOTA_UNIT_ORDER_STOP
		})

		for i=2,#path do
			local order = {
				UnitIndex = self.entindex,
				OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
				Position = GridCellCenter(path[i].x, path[i].y)
			}
			if i > 2 then order.Queue = true end
			ExecuteOrderFromTable(order)
		end

		--AIWorld:DrawDanger()

		return
	end

	-- If safe
	local gameTime = GameRules:GetGameTime()
	if AIWorld:IsSafe(self.hero:GetAbsOrigin()) and self.worldversion ~= AIWorld.version then
		self.worldversion = AIWorld.version

		local paths = AIWorld:GetPathValues(self.hero:GetAbsOrigin())

		local min = 10000
		local max = -10000
		local set = {}

		for path, value in pairs(paths) do
			if value < min then min = value end

			if value > max then
				set = {}
				max = value
				table.insert(set, path)
			end

			if value == max then
				table.insert(set, path)
			end
		end

		local dist = max - min

		for path, value in pairs(paths) do
			local t = (value-min)/dist
			local lastCell = path[#path]
			--DebugDrawText(GridCellCenter(lastCell.x, lastCell.y, 128), value.."", false, 2)
			--DebugDrawBox(GridCellCenter(lastCell.x, lastCell.y, 128),Vector(-80,-80,0), Vector(80,80,1), (1-t)*255, t*255, 0, 150, 2)
		end

		ExecuteOrderFromTable({
			UnitIndex = self.entindex,
			OrderType = DOTA_UNIT_ORDER_STOP
		})

		local path = set[RandomInt(1,#set)]
		if #path > 1 then
			for i=2,#path do
				local order = {
					UnitIndex = self.entindex,
					OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
					Position = GridCellCenter(path[i].x, path[i].y)
				}
				if i > 2 then order.Queue = true end
				ExecuteOrderFromTable(order)
			end
		else
			local order = {
					UnitIndex = self.entindex,
					OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
					Position = GridCellCenter(path[1].x, path[1].y)
				}
				ExecuteOrderFromTable(order)
		end

		self.bombPos = path[#path]
	end

	if self.bombPos and AIWorld:IsSafe(self.hero:GetAbsOrigin()) and (gameTime - self.lastBomb > 3 or gameTime - AIWorld.roundStart > 90) then
		local pos = self.hero:GetAbsOrigin()
		local curCell = WorldToGridCoords(pos.x, pos.y)

		if curCell.x == self.bombPos.x and curCell.y == self.bombPos.y then
			ExecuteOrderFromTable({
				UnitIndex = self.entindex,
				OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
				AbilityIndex = self.hero:FindAbilityByName('place_bomb'):GetEntityIndex(),
				Queue = true
			})
		end
	end
end