-------------------------------------------------------------------------------
--Register event listeners
-------------------------------------------------------------------------------
function BomberGameMode:RegisterListeners()
	ListenToGameEvent( "game_rules_state_change", Dynamic_Wrap( BomberGameMode, 'OnGameStateChanged' ), self )
	ListenToGameEvent( "dota_item_picked_up", Dynamic_Wrap( BomberGameMode, 'OnItemPickup' ), self )
	ListenToGameEvent( "npc_spawned", Dynamic_Wrap( BomberGameMode, 'OnUnitSpawn' ), self )
	ListenToGameEvent( 'dota_player_pick_hero', Dynamic_Wrap( BomberGameMode, 'OnHeroPick' ), self )
	--ListenToGameEvent( "npc_spawned", Dynamic_Wrap( BomberGameMode, 'OnNPCSpawn' ), self )

	CustomGameEventManager:RegisterListener( 'bomber_announce_player', BomberGameMode.OnAnnouncePlayer )
	CustomGameEventManager:RegisterListener( 'player_request_ai', function(sender, params)
		print(params)
		print(self)
		BomberGameMode['OnRequestAI'](self, params)
	end)
end

-------------------------------------------------------------------------------
--EventHandlers
-------------------------------------------------------------------------------
--The UI announces a player
function BomberGameMode:OnAnnouncePlayer( params )
	GameRules.bomberman.playerNames[ params.PlayerID ] = params.name

	GameRules.bomberman.statLog:Add('Players', {
		slot = params.PlayerID,
		name = params.name,
		steamID = PlayerResource:GetSteamAccountID( params.PlayerID )
	})
end

-- AI requested
function BomberGameMode:OnRequestAI( params )
	if GameRules:PlayerHasCustomGameHostPrivileges(PlayerResource:GetPlayer(params.PlayerID)) then
		if GameRules:State_Get() == DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP and self.aiAdded == nil then
			self.aiAdded = true

			if self.numPlayers < 4 then
				print("BOTS")
				for i=self.numPlayers+1, 4 do
						Tutorial:AddBot("npc_dota_hero_techies", "", "", true)
				end

				GameRules:GetGameModeEntity():SetBotThinkingEnabled(false)
				self.numPlayers = 4
			end
		end
	end
end

function BomberGameMode:OnHeroPick( keys )
	if GameRules:State_Get() == DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP then
		local hero = EntIndexToHScript( keys.heroindex )
		local pID = hero:GetPlayerOwnerID()

		-- Set team
		PlayerResource:SetCustomTeamAssignment( pID, DOTA_TEAM_GOODGUYS )
	end
end

--Executed once a unit spawns
function BomberGameMode:OnUnitSpawn( params )
	local unit = EntIndexToHScript( params.entindex )
	if unit:IsRealHero() and unit.initialised ~= true then
		if unit:GetUnitName() ~= "npc_dota_hero_techies" then
			Timers:CreateTimer(RandomFloat(0,1),function()
				local id = unit:GetPlayerOwnerID()
				print("replacing", unit:GetPlayerOwnerID())
				PlayerResource:ReplaceHeroWith(unit:GetPlayerOwnerID(), "npc_dota_hero_techies", 0, 0)
				unit:RemoveSelf()
			end)
		else

			if PlayerResource:IsFakeClient( unit:GetPlayerOwnerID() ) then
				--unit:AddNewModifier(nil, nil, "modifier_command_restrict", {})
				unit:SetBotDifficulty(0)
			end

			unit:RemoveAbility('techies_suicide')
			unit:RemoveAbility('techies_focused_detonate')
			unit:RemoveAbility('techies_minefield_sign')
			unit:RemoveAbility('techies_remote_mines')

			unit:AddNewModifier(nil, nil, 'modifier_stunned', {})

			--set the player's ability points
			unit:SetAbilityPoints(0)
			--level the bomb skill
			local bombAbility = unit:FindAbilityByName('place_bomb')
			bombAbility:SetLevel(1)

			local numPlayers = PlayerResource:GetPlayerCountForTeam( DOTA_TEAM_GOODGUYS )

			unit.initialised = true

			--Tell the UI to update the keybind
			CustomGameEventManager:Send_ServerToPlayer( unit:GetPlayerOwner(), 'bomber_player_spawned', {} )

			self.playersLoaded = self.playersLoaded + 1
			if self.playersLoaded == self.numPlayers then
				Timers:CreateTimer(0.5, function()
					self:AllPlayersJoined()
				end)
			end
		end

		--TODO:ATTACH COLOR PARTICLE
	end
end

--Executed once all players have joined
function BomberGameMode:AllPlayersJoined()
	--loop over all radiant players
	for pID=0, DOTA_MAX_TEAM_PLAYERS - 1 do
		if PlayerResource:IsValidPlayer( pID ) then	
			
			if not self.playerNames[pID] then
				self.playerNames[pID] = PlayerResource:GetPlayerName(pID)
			end

			--set score to 0
			self.playerScores[pID] = 0
			
			--set told to 0
			PlayerResource:SetGold( pID, 0, false)
		end
	end

	local heroes = FindUnitsInRadius(DOTA_TEAM_GOODGUYS, Vector(), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
	for _,hero in pairs(heroes) do
		if hero ~= PlayerResource:GetSelectedHeroEntity(hero:GetPlayerOwnerID()) then
			hero:RemoveSelf()
		end
	end

	--Spawn grid particles
	self:SpawnGridParticles()

	--once all players have heroes, start preparing the first round
	Timers:CreateTimer(0.1, function()
		self:PrepareNextRound()
	end)
end

--Executed every time the gamestate changes, taken from BMD's barebones
function BomberGameMode:OnGameStateChanged( keys )
	local state = GameRules:State_Get()

	if state == DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP then
		
		Timers:CreateTimer(0.1, function()
			local num = 0
			for i=0, DOTA_MAX_TEAM_PLAYERS do
				if PlayerResource:IsValidPlayer(i) then
					print(i)
					num = num + 1
				end
			end
			self.numPlayers = num
			print("players", num)
		end)
	end
end