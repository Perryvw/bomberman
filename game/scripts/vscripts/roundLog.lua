RoundLog = {}
RoundLog.__index = RoundLog

--construct a RoundLog object
function RoundLog.new( roundN )
   local log = {}             -- our new object
   setmetatable(log, RoundLog)
   
   log:init( roundN )
   return log
end

function RoundLog:init( roundN )
	self.logObj = {}
	self.logObj.round = roundN
	self.logObj['players'] = {['type']='list'}
	self.logObj['kills'] = {['type']='list'}
end

function RoundLog:SetStartTime()
	self.logObj['startTime'] = GameRules:GetGameTime()
end

function RoundLog:SetEndTime()
	self.logObj['endTime'] = GameRules:GetGameTime()
end

function RoundLog:AddPlayer( player )
	self.logObj['players'][player] = { slot=player, bombKicks=0, bombsPlaced=0, blastCells=0, powerupDestr=0, cratesDestr=0, powerUps={r=0,g=0,b=0,y=0}}
end

function RoundLog:BombPlace( playerID )
	self.logObj['players'][playerID].bombsPlaced = self.logObj['players'][playerID].bombsPlaced + 1
end

function RoundLog:LogBlast( playerID, blastCells )
	self.logObj['players'][playerID].blastCells = self.logObj['players'][playerID].blastCells + blastCells
end

function RoundLog:CratesDestroyed( playerID, n )
	self.logObj['players'][playerID].cratesDestr = self.logObj['players'][playerID].cratesDestr + n
end

function RoundLog:PowerupsDestroyed( playerID, n )
	self.logObj['players'][playerID].powerupDestr = self.logObj['players'][playerID].powerupDestr + n
end

function RoundLog:PickupPowerup( playerID, powerUp )
	if powerUp == 0 then
		self.logObj['players'][playerID].powerUps.r = self.logObj['players'][playerID].powerUps.r + 1
	elseif powerUp == 1 then
		self.logObj['players'][playerID].powerUps.b = self.logObj['players'][playerID].powerUps.b + 1
	elseif powerUp == 2 then
		self.logObj['players'][playerID].powerUps.y = self.logObj['players'][playerID].powerUps.y + 1
	else
		self.logObj['players'][playerID].powerUps.g = self.logObj['players'][playerID].powerUps.g + 1
	end
end

function RoundLog:BombKick( playerID )
	self.logObj['players'][playerID].bombKicks = self.logObj['players'][playerID].bombKicks + 1
end

function RoundLog:AddKill( killerID, victimID )
	self.logObj['kills'][#self.logObj['kills'] + 1] = {killer=killerID, victim=victimID,t=GameRules:GetGameTime()}
end

function RoundLog:SetWinner( playerName )
	self.logObj['winner'] = playerName
end

function RoundLog:Get()
	return self.logObj
end