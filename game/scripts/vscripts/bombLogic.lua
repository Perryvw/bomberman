-------------------------------------------------------------------------------
--Ability Hooks
-------------------------------------------------------------------------------
--Executed when the player uses the place-bomb ability
function BomberGameMode:onBombPlace( keys )
	--fetch some values
	local heroLoc = keys.caster:GetAbsOrigin()
	local bombCell = WorldToGridCoords(heroLoc.x, heroLoc.y)
	local bombLoc = GridCellCenter(bombCell.x, bombCell.y)

	if keys.caster.onBombPlace then keys.caster.onBombPlace() end
	
	local pID = keys.caster:GetPlayerOwnerID()
	
	--check if the cell is clear
	if self.grid[bombCell.y][bombCell.x] == 0 then
		--check if the player has bombs left to place
		if self.bombsPlaced[pID] < self.bombNum[pID] then
			--make a bomb, set it's owners and add it to some arrays
			local bomb = CreateUnitByName('bomber_bomb', Vector(bombLoc.x, bombLoc.y, heroLoc.z), false, nil, nil, 1)
			bomb:SetOwner( keys.caster )

			--log bomb placement
			self.roundLog:BombPlace( pID )

			self.grid[bombCell.y][bombCell.x] = 1
			self.bombsPlaced[pID] = self.bombsPlaced[pID] + 1
			self.bombs[bombCell.x][bombCell.y] = bomb
			self.lastBomb[pID] = bombCell

			-- HOOK AI INFO
			AIWorld:onBombPlace(bomb)

			--enable the bomb going faster than 522
			bomb:AddNewModifier(bomb, nil, "modifier_bomb_speed", {})
			
			--attach a particle effect to our bombs
			bomb.particleTick = 1
			Timers:CreateTimer(function()
				--check if the bomb hasn't blown up yet
				if not bomb.particleTick then return nil end

				local particleName = "particles/bomb_beep3.vpcf"

				if bomb.particleTick < 4 then
					particleName = "particles/bomb_beep.vpcf"
				elseif bomb.particleTick < 6 then
					particleName = "particles/bomb_beep2.vpcf"
				end

				local particle = ParticleManager:CreateParticle(particleName, PATTACH_ABSORIGIN_FOLLOW, bomb)
				--ParticleManager:SetParticleControl(particle, 1, Vector(7,4,160) )-- set position
				ParticleManager:ReleaseParticleIndex( particle )

				bomb.particleTick = bomb.particleTick + 1

				if bomb.particleTick < 7 then
					return 0.45
				else
					return nil
				end
			end)
			
			--let the UI know the player placed a bomb
			CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( pID ), 'bomber_player_placed_bomb', 
				{total=self.bombNum[pID], available=(self.bombNum[pID]-self.bombsPlaced[pID])} )
			--FireGameEvent("bomber_player_bombs_update", {playerID = pID, total=self.bombNum[pID], available=(self.bombNum[pID]-self.bombsPlaced[pID])})
		end
	end
end

--executed from the modifier's bomb upon running out of the fuse
function BomberGameMode:onBombExplode( keys )
	self:ExplodeBomb(keys.caster, true)
end

--Explode a bomb, parameters are the bomb to explode and if that bomb is the first one in a chain reaction or not
function BomberGameMode:ExplodeBomb( bomb, first )
	--get some values
	local bombPos = bomb:GetAbsOrigin()
	local bombCell = WorldToGridCoords(bombPos.x, bombPos.y)
	local x = bombCell.x
	local y = bombCell.y
	local isFirst = first or false

	-- AI INFO
	AIWorld:onBombExplode(bomb)
	
	--Don't use moving bombs in explosion-chaining unless it's the one starting the chain
	if bomb:GetVelocity():Length() == 0 or isFirst then
		local pID = bomb:GetPlayerOwnerID()
		local power = self.bombPower[pID]
		self.bombsPlaced[pID] = self.bombsPlaced[pID] - 1
		--give the bomb back in the player UI
		CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( pID ), 'bomber_bomb_exploded', 
			{total=self.bombNum[pID], available=(self.bombNum[pID]-self.bombsPlaced[pID])})
		
		--calculate blast radius
		local blastedCells, hitBuildings, hitHeroes, hitBombs, hitPowerups = self:CalculateBlast(x, y, power)
				
		--display the explosion
		self:ShowBlast( blastedCells, bomb:GetAbsOrigin().z, bomb:GetOwner() )
		if isFirst then
			--only play sound on the first bomb so we don't get overlapping/clipping sounds
			EmitSoundOn("Hero_Techies.RemoteMine.Detonate", bomb)
			if #hitBuildings > 1 then
				EmitSoundOn("tutorial_fence_smash", bomb)
			end
		end
				
		--loop over the buildings hit by the explosion
		for _,building in pairs(hitBuildings) do
			--remove the building
			local buildingLoc = building:GetAbsOrigin()
			local buildingCell = WorldToGridCoords(buildingLoc.x, buildingLoc.y)
			self.grid[buildingCell.y][buildingCell.x] = 0
			building:RemoveSelf()
			--possibly drop an item at some location
			if self.dropRNG:Next() then
				self:DropRandomItem( buildingLoc, buildingCell.x, buildingCell.y )
			end
		end

		--Log
		self.roundLog:CratesDestroyed( bomb:GetPlayerOwnerID(), #hitBuildings )
		self.roundLog:PowerupsDestroyed( bomb:GetPlayerOwnerID(), hitPowerups )
		
		--make sure bombs don't go off after the round ended
		if self.gameState == GAME_STATE_ROUND then
			--if a hero is hit by the explosion kill it and announce to al players
			for _,hero in pairs(hitHeroes) do
				if hero:GetPlayerOwnerID() == pID then
					--hero died from his own bomb
					self:AnnounceSuicide( hero )

					--log kill
					self.roundLog:AddKill( pID, pID )
				else
					--hero died from somebody elses bomb
					--give killer credit
					PlayerResource:IncrementKills( pID, 1 )
					--announce kill
					self:AnnounceKill( bomb, hero )

					--log kill
					self.roundLog:AddKill( pID, hero:GetPlayerOwnerID() )
				end
				hero:ForceKill( false )
			end
		end

		--remove this bomb from bomb detection calculation
		local bI = -1
		for i=1,#self.movingBombs do
			if bomb == self.movingBombs[i] then
				bI = i
				break
			end
		end
		
		if bI > -1 then
			table.remove(self.movingBombs, bI)
		end

		--remove the bomb from the game
		self.grid[y][x] = 0
		self.bombs[x][y] = nil

		bomb.particleTick = nil
		bomb:RemoveSelf()
		
		--if other bombs were hit, explode those too
		if #hitBombs > 0 then
			self:ExplodeMoreBombs( hitBombs )
		end
	end
end

--Called when a chain explosion is set in motion
function BomberGameMode:ExplodeMoreBombs( bombs )
	--explode all bombs hit by the explosion
	for _, bombPos in pairs(bombs) do
		local bomb = self.bombs[bombPos.x][bombPos.y]
		if bomb then
			self:ExplodeBomb(bomb, false)
		end
	end
end

--Drop a random item on some location
function BomberGameMode:DropRandomItem(location, cellX, cellY)
	local rune = nil

	--choose an item/rune to drop
	local c = self.dropChoiceRNG:Choose()
	if c == 1 then
		rune = CreateUnitByName('bomber_haste_rune', location, false, nil, nil, DOTA_TEAM_GOODGUYS)
	elseif c == 2 then
		rune = CreateUnitByName('bomber_power_rune', location, false, nil, nil, DOTA_TEAM_GOODGUYS)
	elseif c == 3 then
		rune = CreateUnitByName('bomber_bomb_rune', location, false, nil, nil, DOTA_TEAM_GOODGUYS)
	else
		rune = CreateUnitByName('bomber_kick_rune', location, false, nil, nil, DOTA_TEAM_GOODGUYS)
	end
	
	--add our rune to be used in some calculations/collision checking
	self.grid[cellY][cellX] = 3
	self.drops[cellX][cellY] = rune
end

--This function calculates which cells are affected by a bomb at location x,y with some power and what buildings, 
--heroes, runes and other bombs are hit by the explosion
function BomberGameMode:CalculateBlast(x, y, power)
	--set up a grid representing the map
	local blastedCells = {}
	for cx=0, 12 do
		blastedCells[cx] = {}
	end
	
	--calculate reach of the explosion travelling right from its origin
	for bx=x, math.min(x + 1 + (power-1), 12) do
		if self.grid[y][bx] ~= -1 and self.grid[y][bx] ~= 4 then
			--if the reached cell is not an unsurpassable obstacle, it's hit
			blastedCells[bx][y] = 1
			--if the cell is a barrier stop propagating the explosion in this direction
			if (self.grid[y][bx] == 2) then
				break
			end
		else
			--if the cell IS an unsurpassable obstacle, stop propagation
			break
		end
	end
	
	--calculate the reach of the explosion travelling left from its origin
	for bx=x, math.max(x - 1 - (power-1), 0), -1 do
		if self.grid[y][bx] ~= -1 and self.grid[y][bx] ~= 4 then
			blastedCells[bx][y] = 1
			if (self.grid[y][bx] == 2) then
				break
			end
		else
			break
		end
	end
	
	--calculate the reach of the explosion travelling up
	for by=y, math.min(y + 1 + (power-1), 10) do
		if self.grid[by][x] ~= -1 and self.grid[by][x] ~= 4 then
			blastedCells[x][by] = 1
			if (self.grid[by][x] == 2) then
				break
			end
		else
			break
		end
	end
	
	--calculate the reach travelling down
	for by=y, math.max(y - 1 - (power-1), 0), -1 do
		if self.grid[by][x] ~= -1 and self.grid[by][x] ~= 4 then
			blastedCells[x][by] = 1
			if (self.grid[by][x] == 2) then
				break
			end
		else
			break
		end
	end
	
	--check what we hit
	local hitBuildings = {}
	local hitHeroes = {}
	local hitBombs = {}
	local hitPowerups = 0
	
	for cx=0, 12 do
		for cy=0,10 do
			--only look at cells hit by the explosion
			if blastedCells[cx][cy] then
				--look for bombs
				if self.bombs[cx][cy] and not (cx == x and cy==y) then
					hitBombs[#hitBombs+1] = {x=cx, y=cy}
				end
				--look for buildings
				if self.grid[cy][cx] == 2 then
					--get the building
					local buildingLoc = GridCellCenter(cx, cy, GROUND_LEVEL)
					hitBuildings[#hitBuildings+1] = Entities:FindByClassnameNearest('npc_dota_creature', buildingLoc, 10)
				end

				--look for drops
				if self.grid[cy][cx] == 3 then
					--remove any hit drops
					local drop = self.drops[cx][cy]
					drop:RemoveSelf()
					self.grid[cy][cx] = 0
					self.drops[cx][cy] = nil
					hitPowerups = hitPowerups + 1
				end
			end
		end
	end
	
	--check what heroes we hit
	for _,hero in pairs(self.playerHeroes) do
		if hero:IsAlive() then
			local heroPos = hero:GetAbsOrigin()
			local curCell = WorldToGridCoords(heroPos.x, heroPos.y)
			if blastedCells[curCell.x][curCell.y] then
				hitHeroes[#hitHeroes+1] = hero
			end
		end
	end
	
	return blastedCells, hitBuildings, hitHeroes, hitBombs, hitPowerups
end

--Displays a blast in a pattern described by a grid of cells. Cell=0: no explosion there, Cell=1: boom!
function BomberGameMode:ShowBlast( grid, bomb, owner )
	local n = 0
	for x=0, 12 do
		for y=0,10 do
			if grid[x][y] then
				local particle = ParticleManager:CreateParticle("particles/units/heroes/hero_techies/techies_land_mine_explode.vpcf", PATTACH_CUSTOMORIGIN, owner)
				ParticleManager:SetParticleControl(particle, 0, GridCellCenter(x, y, 64) )-- set position
				ParticleManager:ReleaseParticleIndex( particle )
				n = n+1
			end
		end
	end
	self.roundLog:LogBlast( owner:GetPlayerID(), n )
end

--This function initiates a bomb-kick by a hero in some cell
function BomberGameMode:KickBomb( hero, bombCell )
	--get the bomb and soem values
	local bomb = self.bombs[bombCell.x][bombCell.y]
	local diffVec = vectorMinus(bomb:GetAbsOrigin(), hero:GetAbsOrigin())
	local heroPos = hero:GetAbsOrigin()
	--check from what side the hero is kicking the bomb
	if math.abs(diffVec.x) > math.abs(diffVec.y) then
		--Find target cells for the bombs
		if diffVec.x > 0 then
			--move right
			local targetX = 12
			for cx=bombCell.x + 1, 12 do
				if self.grid[bombCell.y][cx] ~= 0 then
					targetX = cx-1
					break
				end
			end
			if targetX ~= bombCell.x then
				--If the target cell is not the current cell make the bomb start moving towards that cell
				bomb:MoveToPosition(GridCellCenter(targetX, bombCell.y, heroPos.z))
				table.insert(self.movingBombs, bomb)
				bomb.moveStart = GameRules:GetGameTime()
				--previous location needed for collision calculations
				bomb.prevLoc = bomb:GetAbsOrigin()
				--turn it the right way
				bomb:SetForwardVector((GridCellCenter(targetX, bombCell.y, heroPos.z)-bomb:GetAbsOrigin()):Normalized())
				self.grid[bombCell.y][bombCell.x] = 0
				self.bombs[bombCell.x][bombCell.y] = nil

				--log kick
				self.roundLog:BombKick( hero:GetPlayerOwnerID() )
			elseif diffVec:Dot(hero:GetForwardVector()) > 0 then
				--if the target cell IS the current cell and the hero is trying to kick it, move the hero off of the bomb
				--and don't move the bomb
				FindClearSpaceForUnit(hero, bomb:GetAbsOrigin() - diffVec*((BOMB_RADIUS)/diffVec:Length()), false)
				hero:Hold()
			end
		else
			--move left
			local targetX = 0
			for cx=bombCell.x - 1, 0, -1 do
				if self.grid[bombCell.y][cx] ~= 0 then
					targetX = cx+1
					break
				end
			end
			if targetX ~= bombCell.x then
				bomb:MoveToPosition(GridCellCenter(targetX, bombCell.y, heroPos.z))
				table.insert(self.movingBombs, bomb)
				bomb.moveStart = GameRules:GetGameTime()
				bomb.prevLoc = bomb:GetAbsOrigin()
				bomb:SetForwardVector((GridCellCenter(targetX, bombCell.y, heroPos.z)-bomb:GetAbsOrigin()):Normalized())
				self.grid[bombCell.y][bombCell.x] = 0
				self.bombs[bombCell.x][bombCell.y] = nil

				--log kick
				self.roundLog:BombKick( hero:GetPlayerOwnerID() )
			elseif diffVec:Dot(hero:GetForwardVector()) > 0 then
				FindClearSpaceForUnit(hero, bomb:GetAbsOrigin() - diffVec*((BOMB_RADIUS)/diffVec:Length()), false)
				hero:Hold()
			end
		end
	else 
		--move vertical
		if diffVec.y > 0 then
			--move up
			local targetY = 10
			for cy=bombCell.y + 1, 10 do
				if self.grid[cy][bombCell.x] ~= 0 then
					targetY = cy-1
					break
				end
			end
			if targetY ~= bombCell.y then
				bomb:MoveToPosition(GridCellCenter(bombCell.x, targetY, heroPos.z))
				table.insert(self.movingBombs, bomb)
				bomb.moveStart = GameRules:GetGameTime()
				bomb.prevLoc = bomb:GetAbsOrigin()
				bomb:SetForwardVector((GridCellCenter(bombCell.x, targetY, heroPos.z)-bomb:GetAbsOrigin()):Normalized())
				self.grid[bombCell.y][bombCell.x] = 0
				self.bombs[bombCell.x][bombCell.y] = nil

				--log kick
				self.roundLog:BombKick( hero:GetPlayerOwnerID() )
			elseif diffVec:Dot(hero:GetForwardVector()) > 0 then
				FindClearSpaceForUnit(hero, bomb:GetAbsOrigin() - diffVec*((BOMB_RADIUS)/diffVec:Length()), false)
				hero:Hold()
			end
		else
			--move down
			local targetY = 0
			for cy=bombCell.y - 1, 0, -1 do
				if self.grid[cy][bombCell.x] ~= 0 then
					targetY = cy+1
					break
				end
			end
			if targetY ~= bombCell.y then
				bomb:MoveToPosition(GridCellCenter(bombCell.x, targetY, heroPos.z))
				table.insert(self.movingBombs, bomb)
				bomb.moveStart = GameRules:GetGameTime()
				bomb.prevLoc = bomb:GetAbsOrigin()
				bomb:SetForwardVector((GridCellCenter(bombCell.x, targetY, heroPos.z)-bomb:GetAbsOrigin()):Normalized())
				self.grid[bombCell.y][bombCell.x] = 0
				self.bombs[bombCell.x][bombCell.y] = nil

				--log kick
				self.roundLog:BombKick( hero:GetPlayerOwnerID() )
			elseif diffVec:Dot(hero:GetForwardVector()) > 0 then
				FindClearSpaceForUnit(hero, bomb:GetAbsOrigin() - diffVec*((BOMB_RADIUS)/diffVec:Length()), false)
				hero:Hold()
			end
		end
	end
end