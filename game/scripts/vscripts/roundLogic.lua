--This function prepares the next round as in it respawns all players, resets the grid, bombs and powerups
--and does some timing stuff. Most importantly aranges spawn locations.
function BomberGameMode:PrepareNextRound()
	self.gameState = GAME_STATE_BETWEEN_ROUNDS
	self.currentRound = self.currentRound + 1
	self.roundStartTime = GameRules:GetGameTime() + 3
	self:ClearMap()

	AIWorld.roundStart = self.roundStartTime

	--Build a list of all heroes that will participate
	self.playerHeroes = {}
	for i= 0,DOTA_MAX_TEAM_PLAYERS - 1 do
		if PlayerResource:IsValidPlayer( i ) or PlayerResource:IsFakeClient( i ) then
			local hero = PlayerResource:GetSelectedHeroEntity( i )
			if hero.ai ~= nil then
				hero.ai:Enabled(false)
			end

			if PlayerResource:IsFakeClient( i ) or PlayerResource:GetConnectionState( i ) == 3 then
				if hero.ai ~= nil then
					hero.ai:Enabled(true)
				else
					hero.ai = bombAI(hero)
					hero.ai:Enabled(true)
				end
			end
			table.insert( self.playerHeroes, hero )
		end
	end

	--update HUD
	ShowCenterMessage( 'Round '..self.currentRound )
	CustomGameEventManager:Send_ServerToAllClients( 'bomber_bombs_reset', {} )
	
	--if this is the first round we're preparing do some extra stuff
	if self.currentRound == 1 then
		Timers:CreateTimer({
			endTime = 1,
			callback = function()
				FireGameEvent( "bomber_start_game", {} )
				return nil
			end
		})
		for _,hero in pairs(self.playerHeroes) do
			FireGameEvent("bomber_player_join", {slot = hero:GetPlayerOwnerID()})
		end
	end
	
	--reset the grid
	self.grid = {
		[0] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=2, [6]=2, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[1] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=2, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[2] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=2, [6]=2, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[3] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=2, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[4] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=0, [6]=0, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[5] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=0, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[6] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=2, [6]=2, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[7] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=2, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[8] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=2, [6]=2, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[9] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=2, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[10] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=2, [6]=2, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2}
	}
	
	local heroI = 0
	
	self.roundLog = RoundLog.new(self.currentRound-1)

	for _,hero in pairs(self.playerHeroes) do
		if not hero:IsAlive() then
			--if the hero is dead respawn it first of all
			hero:RespawnHero(false, false)
		end
		
		--stun the hero and get rid of any speed modifiers
		hero:AddNewModifier(nil, nil, 'modifier_stunned', {})
		if hero:HasModifier('modifier_bomber_speed_1') then
			hero:RemoveModifierByName('modifier_bomber_speed_1')
		elseif hero:HasModifier('modifier_bomber_speed_2') then
			hero:RemoveModifierByName('modifier_bomber_speed_2')
		elseif hero:HasModifier('modifier_bomber_speed_3') then
			hero:RemoveModifierByName('modifier_bomber_speed_3')
		elseif hero:HasModifier('modifier_bomber_speed_4') then
			hero:RemoveModifierByName('modifier_bomber_speed_4')
		elseif hero:HasModifier('modifier_bomber_speed_5') then
			hero:RemoveModifierByName('modifier_bomber_speed_5')
		elseif hero:HasModifier('modifier_bomber_speed_6') then
			hero:RemoveModifierByName('modifier_bomber_speed_6')
		end
		
		--set the hero spawn and upgrade the grid for it
		if heroI == 0 then
			hero:SetAbsOrigin(GridCellCenter(0, 0, hero:GetAbsOrigin().z))
			self.grid[0][0] = 0
			self.grid[0][1] = 0
			self.grid[1][0] = 0
			self.grid[4][5] = 2
			self.grid[4][6] = 2
			self.grid[5][6] = 2
		elseif heroI == 1 then
			hero:SetAbsOrigin(GridCellCenter(12, 0, hero:GetAbsOrigin().z))
			self.grid[0][12] = 0
			self.grid[0][11] = 0
			self.grid[1][12] = 0
		elseif heroI == 2 then
			hero:SetAbsOrigin(GridCellCenter(0, 10, hero:GetAbsOrigin().z))
			self.grid[10][0] = 0
			self.grid[10][1] = 0
			self.grid[9][0] = 0
		elseif heroI == 3 then
			hero:SetAbsOrigin(GridCellCenter(12, 10, hero:GetAbsOrigin().z))
			self.grid[10][12] = 0
			self.grid[9][12] = 0
			self.grid[10][11] = 0
		elseif heroI == 4 then
			hero:SetAbsOrigin(GridCellCenter(6, 0, hero:GetAbsOrigin().z))
			self.grid[0][6] = 0
			self.grid[0][5] = 0
			self.grid[0][7] = 0
			self.grid[1][6] = 0
		elseif heroI == 5 then
			hero:SetAbsOrigin(GridCellCenter(6, 10, hero:GetAbsOrigin().z))
			self.grid[10][6] = 0
			self.grid[10][5] = 0
			self.grid[10][7] = 0
			self.grid[9][6] = 0
		end
		
		--reset all powerups picked up by the hero
		local pID = hero:GetPlayerOwnerID()
		self.roundLog:AddPlayer( pID )
		PlayerResource:SetCameraTarget( pID, hero )
		self.bombPower[pID] = 1
		self.bombNum[pID] = 1
		self.remoteBombs[pID] = false
		self.kickBombs[pID] = false
		
		self.bombsPlaced[pID] = 0
		self.lastBomb[pID] = bombCell
		heroI = heroI + 1
	end
	
	self:SpawnBarriers()
end

--Start the prepared round
function BomberGameMode:StartRound()
	--Start the round and show a message in the HUD
	ShowCenterMessage('Start!', 2)
	self.gameState = GAME_STATE_ROUND
	
	self.roundLog:SetStartTime()

	--Start HUD timer
	CustomGameEventManager:Send_ServerToAllClients( 'bomber_round_start', {} )

	--Start a timer for the timeout
	Timers:CreateTimer('RoundEndTimer', {
			endTime = 90,
			callback = function()
				GameRules.bomberman:InitRoundEndSeq()
				return nil
			end
		})

	--Free the stunned heroes and unlock their camera
	for _,hero in pairs(self.playerHeroes) do
		hero:RemoveModifierByName('modifier_stunned')
		local pID = hero:GetPlayerOwnerID()
		PlayerResource:SetCameraTarget( pID, nil )
	end
end

--The beginning of the end! (of the round)
function BomberGameMode:InitRoundEndSeq()
	--Show HUD message
	ShowCenterMessage('Hurry up!', 2)

	--Start a spiral loop traversing our grid
	local x = 0
	local y = 0
	local dy = 1
	local dx = 0
	local W = 13
	local H = 11
	local Up = H - 1
	local Down = -1
	local Left = 0
	local Right = W - 1
	local n = 0

	while n < W*H do
		--DO STUFF WITH x and y here
		local lx = x + 0
		local ly = y + 0
		--Set a timer to disable a cell at some time
		Timers:CreateTimer("bombExplode"..x..'x'..y, {
			endTime = 0.6*n,
			callback = function()
				GameRules.bomberman:BlockCell(lx, ly)
			end
		})

		--DONT TOUCH THIS ANYMORE!!!!
		--SPiral logic
		if dy == 1 and y == Up then
			dy = 0
			dx = 1
			Down = Down + 1
		elseif dy == -1 and y == Down then
			dy = 0
			dx = -1
			Up = Up - 1
		end

		if dx == 1 and x == Right then
			dx = 0
			dy = -1
			Left = Left + 1
		elseif dx == -1 and x == Left then
			dx = 0
			dy = 1
			Right = Right - 1
		end

		if Right < Left or Up < Down then
			break
		end

		if Right == 9 then
			break
		end

		x = x + dx
		y = y + dy

		n = n + 1
	end

	--Need this to stop barriers from spawning in the next round
	self.endingRound = self.currentRound

	--Set timeout for draw
	Timers:CreateTimer('ForceRoundEndTimer', { 
		endTime = 90,
		callback = function()
			GameRules.bomberman:ForceRoundEnd()
		end
	})
end

--This functions blocks a cell in our grid and kills any heroes on it
function BomberGameMode:BlockCell(cx, cy)
	if self.grid[cy][cx] ~= -1 and self.gameState == GAME_STATE_ROUND and self.endingRound == self.currentRound then
		--If there is anything on this cell remove it
		if self.grid[cy][cx] == 2 or self.grid[cy][cx] == 3 then
			self.grid[cy][cx] = 0
			local buildingLoc = GridCellCenter(cx, cy, GROUND_LEVEL)
			local entity = Entities:FindByClassnameNearest('npc_dota_creature', buildingLoc, 10)
			if entity then
				entity:RemoveSelf()
			end
		end

		--Show a lill particle effect
		local particle = ParticleManager:CreateParticle("particles/units/heroes/hero_spirit_breaker/spirit_breaker_greater_bash.vpcf", PATTACH_CUSTOMORIGIN, nil)
		ParticleManager:SetParticleControl(particle, 0, GridCellCenter(cx, cy, 64) )-- set position
		ParticleManager:ReleaseParticleIndex( particle )

		--create a barrier and play a sound
		local barrier = CreateUnitByName('bomber_barricade_invul', GridCellCenter(cx,cy,GROUND_LEVEL), false, nil, nil, DOTA_TEAM_BADGUYS)
		barrier:SetHullRadius( 120 )
		EmitSoundOn("Hero_Spirit_Breaker.GreaterBash.Creep", barrier)
		self.bombs[cx][cy] = barrier
		self.grid[cy][cx] = 4

		--If there is any hero on the to-be-disabled cell kill it
		for _,hero in pairs(self.playerHeroes) do
			if hero:IsAlive() then
				local heroPos = hero:GetAbsOrigin()
				local curCell = WorldToGridCoords(heroPos.x, heroPos.y)
				if curCell.x == cx and curCell.y == cy then
					self:AnnounceSuicide( hero )
					hero:ForceKill( false )

					--log death
					self.roundLog:AddKill( -1, hero:GetPlayerOwnerID() )
				end
			end
		end
	end
end

--This function counts down from 10 to 1 and then ends the round in a draw
function BomberGameMode:ForceRoundEnd()
	--start the countdown, make a timer that fires every second
	local countDown = 10
	Timers:CreateTimer('ForceRoundEndTimer',{
		endTime = 1,
		callback = function()
			if countDown > 0 then
				--show the countdown number
				ShowCenterMessage(countDown..'', 1)
				countDown = countDown -1
				return 1
			else
				--if we're at 0, end the round in a draw
				local self = GameRules.bomberman
				self.gameState = GAME_STATE_ROUND_END
				self.nextRoundTime = GameRules:GetGameTime() + 6
				Timers:RemoveTimer('RoundEndTimer')
				Timers:RemoveTimer('ForceRoundEndTimer')
				
				--Show a draw message on HUD
				ShowCenterMessage('Draw!')

				--Do some fun stuff with remaining heroes
				for _,hero in pairs(self.playerHeroes) do
					if hero:IsAlive() then
						hero:AddNewModifier(nil, nil, 'modifier_stunned', {})
						hero:AddNewModifier(hero, nil, 'modifier_tutorial_forceanimation', {duration=5.9, loop=1, activity=ACT_DOTA_FLAIL})
					end
				end

				return nil
			end
		end
	})			
end

--Get rid of all bariers, runes, bombs on the map
function BomberGameMode:ClearMap()
	local ents = Entities:FindAllByClassname('npc_dota_creature')
	for _, ent in pairs(ents) do
		ent:RemoveSelf()
	end
	
	for x=0, 12 do
		for y=0, 10 do
			self.bombs[x][y] = nil
		end
	end
end

function BomberGameMode:RoundThink()
	--if we're in the round state, do a check if there is only 1 or no players still alive
	local aliveN = 0
	local aliveHero = nil
	
	for _,hero in pairs(self.playerHeroes) do
		local pID = hero:GetPlayerOwnerID()
		local cell = self.lastBomb[pID]
		
		if hero:IsAlive() then
			aliveN = aliveN+1
			aliveHero = hero
		end
		
		--check if the hero is still on its last placed bomb or not so we can enable him to kick it
		if cell then
			local heroPos = hero:GetAbsOrigin()
			--check if the bomb is still there
			local bomb = self.bombs[cell.x][cell.y]
			if bomb then
				local bombPos = self.bombs[cell.x][cell.y]:GetAbsOrigin()
				local dist = math.sqrt((heroPos.x-bombPos.x)*(heroPos.x-bombPos.x) + (heroPos.y-bombPos.y)*(heroPos.y-bombPos.y))
				if (dist > BOMB_RADIUS) then
					--player has moved off of the bomb so enable collision checking for this bomb
					self.lastBomb[pID] = nil
				end
			else
				--bomb is gone so we remove the last bomb
				self.lastBomb[pID] = nil
			end
		end
	end

	--if there is only one hero alive he wins
	if aliveN == 1 and self.numPlayers > 1 then
		--end the round
		self.gameState = GAME_STATE_ROUND_END
		self.nextRoundTime = GameRules:GetGameTime() + 5

		--End the Timer in the HUD
		CustomGameEventManager:Send_ServerToAllClients( 'bomber_round_end', {} )
		
		--show visual and sound in game
		--ShowCenterMessage(self.playerNames[aliveHero:GetPlayerOwnerID()]..' wins!')
		self.playerScores[aliveHero:GetPlayerOwnerID()] = self.playerScores[aliveHero:GetPlayerOwnerID()] + 1

		--Notify Hud of player score
		CustomGameEventManager:Send_ServerToAllClients( 'bomber_score_update', { scores = self.playerScores } );

		if not IsInToolsMode() then
			GameRules.Winner = aliveHero:GetPlayerOwnerID()
		end

		if self.playerScores[aliveHero:GetPlayerOwnerID()] == 5 then

			statCollection:submitRound(true)

			--Gather log information
			self.roundLog:SetWinner(aliveHero:GetPlayerOwnerID())
			self.roundLog:SetEndTime()
			self.statLog:Add( 'matchID', GetNormTime()..math.random(0,9)..math.random(0,9) )
			self.statLog:Add( 'Rounds', self.roundLog:Get() )
			self.statLog:Add('Winner', aliveHero:GetPlayerOwnerID())

			--send log
			--[[if not IsInToolsMode() then
				local logstr = self.statLog:ToJSON()
				print(logstr)

				local request = CreateHTTPRequest( 'POST', 'http://yrrep.me/dota/bomberman/logs.php' )
				request:SetHTTPRequestGetOrPostParameter( 'log', logstr )

				request:Send( function( response )
					DeepPrintTable( response )
				end)
			end]]
			
			--end the game
			GameRules:SetCustomVictoryMessage(PlayerResource:GetPlayerName(aliveHero:GetPlayerOwnerID())..' wins!')
			GameRules:SetGameWinner(DOTA_TEAM_GOODGUYS)
		else
			if not IsInToolsMode() then
				statCollection:submitRound(false)
			end
		end
		EmitGlobalSound("crowd.lv_04")
		
		--update scoreboard
		FireGameEvent("bomber_point_scored", {ID = aliveHero:GetPlayerOwnerID()})

		--remove the timelimit timers
		Timers:RemoveTimer('RoundEndTimer')
		Timers:RemoveTimer('ForceRoundEndTimer')
		
		--make the winning hero cheer
		aliveHero:AddNewModifier(aliveHero, nil, 'modifier_tutorial_forceanimation', {duration=7.9, loop=1, activity=ACT_DOTA_CAST_ABILITY_6})
		
		--force the others to look at it
		for _,lhero in pairs(self.playerHeroes) do
			PlayerResource:SetCameraTarget( lhero:GetPlayerOwnerID(), aliveHero)
		end

		--Gather log information
		self.roundLog:SetWinner(aliveHero:GetPlayerOwnerID())
		self.roundLog:SetEndTime()
		self.statLog:Add( 'Rounds', self.roundLog:Get() )
	end

	--if not players are left alive it's a draw
	if aliveN == 0 then
		if not IsInToolsMode() then
			GameRules.Winner = -2
			statCollection:submitRound(false)
		end

		self.gameState = GAME_STATE_ROUND_END
		self.nextRoundTime = GameRules:GetGameTime() + 5
		Timers:RemoveTimer('RoundEndTimer')
		Timers:RemoveTimer('ForceRoundEndTimer')
		
		ShowCenterMessage('Draw!')

		--End the Timer in the HUD
		CustomGameEventManager:Send_ServerToAllClients( 'bomber_round_end', {} )

		--Update HUD
		CustomGameEventManager:Send_ServerToAllClients( 'bomber_score_update', { scores = self.playerScores } );

		--Gather log information
		self.roundLog:SetWinner(-1)
		self.roundLog:SetEndTime()
		self.statLog:Add( 'Rounds', self.roundLog:Get() )
	end
end