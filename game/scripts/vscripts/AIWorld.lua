if AIWorld == nil then
	AIWorld = class({})
end

function AIWorld:Init()
	self = AIWorld

	self.version = 0
	self.danger = {}

	self.bombs = {}

	self.changed = false
end

function AIWorld:onBombPlace(bomb)
	self.bombs[bomb] = true
	self.changed = true
	self.version = self.version + 1
end

function AIWorld:onBombExplode(bomb)
	self.bombs[bomb] = nil
	self.changed = true
	self.version = self.version + 1
end

function AIWorld:UpdateDangerMap(map)
	self.map = map
	if self.changed == false then return end

	self.changed = false

	self.danger = {}

	for bomb, _ in pairs(self.bombs) do
		local pos = bomb:GetAbsOrigin()
		local cell = WorldToGridCoords(pos.x, pos.y)

		self.danger[self:cellHash(cell.x, cell.y)] = true

		local i = 1
		while map[cell.y][cell.x + i] == 0 or map[cell.y][cell.x + i] == 3 do
			self.danger[self:cellHash(cell.x + i, cell.y)] = true
			i = i + 1
		end

		local i = 1
		while map[cell.y][cell.x - i] == 0 or map[cell.y][cell.x - i] == 3 do
			self.danger[self:cellHash(cell.x - i, cell.y)] = true
			i = i + 1
		end

		local i = 1
		while map[cell.y + i] ~= nil and (map[cell.y + i][cell.x] == 0 or map[cell.y + i][cell.x] == 3) do
			self.danger[self:cellHash(cell.x, cell.y + i)] = true
			i = i + 1
		end

		local i = 1
		while map[cell.y - i] ~= nil and (map[cell.y - i][cell.x] == 0 or map[cell.y - i][cell.x] == 3) do
			self.danger[self:cellHash(cell.x, cell.y - i)] = true
			i = i + 1
		end
	end
end

function AIWorld:cellHash(x, y)
	return 1000 * x + y
end

function AIWorld:IsSafe(pos)
	local cell = WorldToGridCoords(pos.x, pos.y)
	return self:CellSafe(cell.x, cell.y)
end

function AIWorld:CellSafe(x, y)
	return self.danger[self:cellHash(x, y)] ~= true
end

function AIWorld:GetShortestSafetyPath(currentPos)
	local cell = WorldToGridCoords(currentPos.x, currentPos.y)

	if self:CellSafe(cell.x, cell.y) then
		return {}
	end

	-- Breadth-First Search
	local seen = {}
	local currentL = {{cell}}
	local nextL = {}

	while #currentL > 0 or #nextL > 0 do

		local curPath = currentL[#currentL]
		currentL[#currentL] = nil

		local pos = curPath[#curPath]
		seen[self:cellHash(pos.x, pos.y)] = true

		-- Random sequence of checking
		local choices = {1,2,3,4}
		for i=1,4 do
			local rand = RandomInt(i,4)
			local choice = choices[rand]
			local old = choices[i]
			choices[i] = choices[rand]
			choices[rand] = old

			if choice == 1 then
				local path = self:explorePath(curPath, pos.x + 1, pos.y, seen, nextL)
				if path ~= nil then return path end
			end

			if choice == 2 then
				local path = self:explorePath(curPath, pos.x - 1, pos.y, seen, nextL)
				if path ~= nil then return path end
			end

			if choice == 3 then
				local path = self:explorePath(curPath, pos.x, pos.y + 1, seen, nextL)
				if path ~= nil then return path end
			end

			if choice == 4 then
				local path = self:explorePath(curPath, pos.x, pos.y - 1, seen, nextL)
				if path ~= nil then return path end
			end
		end

		if #currentL == 0 then
			currentL = nextL
			nextL = {}
		end
	end

	return {}
end

function AIWorld:explorePath(curPath, x, y, seen, nextL)
	if seen[self:cellHash(x, y)] ~= true and self.map[y] ~= nil and (self.map[y][x] == 0 or self.map[y][x] == 3 or self.map[y][x] == 1) then
		-- Check if safe
		if self:CellSafe(x, y) then
			table.insert(curPath, Vector(x, y))
			return curPath
		end

		local newPath = {}
		for _,p in pairs(curPath) do table.insert(newPath, p) end

		table.insert(newPath, Vector(x, y))
		table.insert(nextL, newPath)
	end
end

function AIWorld:GetPathValues(currentPos)
	local cell = WorldToGridCoords(currentPos.x, currentPos.y)

	local MAX_SEARCH_DIST = 5

	-- Breadth-First Search
	local startPath = {Vector(cell.x, cell.y)}
	local startHash = self:cellHash(cell.x, cell.y)
	local seen = {[startHash] = startPath}
	local values = {[startHash] = self:GetCellValue(0, cell.x, cell.y)}

	local currentL = {startPath}
	local nextL = {}
	local dist = 1

	while #currentL > 0 or #nextL > 0 do

		local curPath = currentL[#currentL]
		currentL[#currentL] = nil

		local pos = curPath[#curPath]
		--seen[self:cellHash(pos.x, pos.y)] = true

		self:explorePathValue(curPath, pos.x + 1, pos.y, seen, nextL, values)
		self:explorePathValue(curPath, pos.x - 1, pos.y, seen, nextL, values)
		self:explorePathValue(curPath, pos.x, pos.y + 1, seen, nextL, values)
		self:explorePathValue(curPath, pos.x, pos.y - 1, seen, nextL, values)

		if #currentL == 0 then
			if dist < MAX_SEARCH_DIST then
				currentL = nextL
			end
			nextL = {}
			dist = dist + 1
		end
	end

	local returnTable = {}
	for hash, path in pairs(seen) do
		returnTable[path] = values[hash]
	end

	return returnTable
end

function AIWorld:explorePathValue(curPath, x, y, seen, nextL, values)
	local hash = self:cellHash(x,y)
	if seen[hash] == nil and self.map[y] ~= nil and (self.map[y][x] == 0 or self.map[y][x] == 3) then
		-- Get value
		local origin = curPath[#curPath]
		local value = self:GetCellValue(values[self:cellHash(origin.x, origin.y)], x, y)

		-- Copy path
		local newPath = {}
		for _,p in ipairs(curPath) do
			table.insert(newPath, p)
		end
		-- Add new cell
		table.insert(newPath, Vector(x, y))

		-- Store value
		values[hash] = value
		seen[hash] = newPath

		table.insert(nextL, newPath)
	end
end

function AIWorld:GetCellValue(cellValue, x, y)
	-- Config
	local VALUE_MOVE = -2
	local VALUE_POWERUP = 5
	local VALUE_CRATE = 3
	local VALUE_UNSAFE = -100

	-- Subtract 1 for having to move
	local value = cellValue + VALUE_MOVE

	-- Bonus for having a powerup
	if self.map[y][x] == 3 then value = value + VALUE_POWERUP end

	-- Bonus for each adjacent crate
	if self.map[y][x+1] == 2 then value = value + VALUE_CRATE end
	if self.map[y][x-1] == 2 then value = value + VALUE_CRATE end
	if self.map[y-1] ~= nil and self.map[y-1][x] == 2 then value = value + VALUE_CRATE end
	if self.map[y+1] ~= nil and self.map[y+1][x] == 2 then value = value + VALUE_CRATE	end

	if self:CellSafe(x, y) ~= true then value = value + VALUE_UNSAFE end

	return value
end

--DEBUGGING
function AIWorld:PrintMap(map)
	for y=0,10 do
		local str = ""
		for x=0,12 do
			if map[10-y][x] < 0 then
				str = str..map[10-y][x].."   "
			else
				str = str..map[10-y][x].."    "
			end
		end
		print(str)
	end
end

function AIWorld:DrawDanger()
	for x=0,12 do
		for y=0,10 do
			local center = GridCellCenter(x,y,128)
			if self.map[y][x] ~= 0 and self.map[y][x] ~= 3 then
				DebugDrawBox(center,Vector(-70,-70,0), Vector(70,70,1), 150, 150, 150, 20, 2)
			else
				if self:CellSafe(x,y) then
					DebugDrawBox(center,Vector(-70,-70,0), Vector(70,70,1), 0, 255, 0, 20, 2)
				else
					DebugDrawBox(center,Vector(-70,-70,0), Vector(70,70,1), 255, 0, 0, 20, 2)
				end
			end
		end
	end
end