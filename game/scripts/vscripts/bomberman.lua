if BomberGameMode == nil then
	BomberGameMode = class({})
end

------------------------------------------------------------------------------
--Global Variables
--------------------------------------------------------------------------------
DROP_CHANCE = 0.50	--chance an item drops upon barrier destruction

ITEM_DROP_CHANCE = {	--the chance a drop is a certain powerup (try to keep total 1)
	speed = 0.30,
	power = 0.30,
	bombs = 0.30,
	kick = 0.10
}

BOMB_RADIUS = 130		--radius of bombs, used in collision detection
GROUND_LEVEL = 105		--ground level, used in position calculations

--game state definitions
GAME_STATE_PRE_BOMB = 0
GAME_STATE_BETWEEN_ROUNDS  = 1
GAME_STATE_ROUND = 2
GAME_STATE_ROUND_END = 3

--Link lua modifiers
LinkLuaModifier( "modifier_bomb_speed", "LuaModifiers/modifier_bomb_speed.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_command_restrict", "LuaModifiers/modifier_command_restrict.lua", LUA_MODIFIER_MOTION_NONE )

--testing?
TESTING = false
SPAWN_AI = false
--------------------------------------------------------------------------------
--Initialise custom game mode
--------------------------------------------------------------------------------
function BomberGameMode:InitGameMode()
    print( "Initialising mode!" )
	
	--set a decent random seed
	local timeTxt = string.gsub(string.gsub(GetSystemTime(), ':', ''), '0','')
	math.randomseed(tonumber(timeTxt))

	GameRules:SetSameHeroSelectionEnabled(true)
    
	self.numPlayers = 0

	self.statLog = LOG.New()
	
	--set up our grid
	self.grid = {
		[0] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=2, [6]=2, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[1] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=2, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[2] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=2, [6]=2, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[3] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=2, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[4] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=0, [6]=0, [7]=0, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[5] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=0, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[6] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=0, [6]=0, [7]=0, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[7] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=2, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[8] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=2, [6]=2, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2},
		[9] = {[0]=2, [1]=-1, [2]=2, [3]=-1, [4]=2, [5]=-1, [6]=2, [7]=-1, [8]=2, [9]=-1, [10]=2, [11]=-1, [12]=2},
		[10] = {[0]=2, [1]=2, [2]=2, [3]=2, [4]=2, [5]=2, [6]=2, [7]=2, [8]=2, [9]=2, [10]=2, [11]=2, [12]=2}
	}

	-- Set up AI world
	AIWorld:Init()
	
	--init round logic
	self.gameState = GAME_STATE_PRE_BOMB
	self.currentRound = 0
	self.roundStartTime = 1000
	self.nextRoundTime = 1000
	self.endingRound = 0
	
	--set up bomb array
	self.bombs = {}
	for i=0,12 do
		self.bombs[i] = {}
	end
	
	--set up drop array
	self.drops = {}
	for i=0,12 do
		self.drops[i] = {}
	end
	
	--player names stored here
	self.playerNames = {}

	--keep score
	self.playerScores = {}
	
	--player upgrade levels
	self.bombPower = {}
	self.bombNum = {}
	self.remoteBombs = {}
	self.kickBombs = {}
	
	--extra info per player
	self.bombsPlaced = {}
	self.lastBomb = {}

	--player heroes
	self.playersLoaded = 0
	self.playerHeroes = {}

	--setup for bomb collision detection
	self.movingBombs = {}
	
	--initialise our pseudo random number generators for drops
	self:InitDropRNGs()
	
	--set up Event listeners
	self:RegisterListeners()
	
	--Start thinking
	Timers:CreateTimer( BomberGameMode.Think, self )
	Timers:CreateTimer( BomberGameMode.PhysicsThink, self )
	
	--set up our view
	local gameBase = GameRules:GetGameModeEntity()
	gameBase:SetCameraDistanceOverride(1700)
	gameBase:SetFogOfWarDisabled(true)
	gameBase:SetCustomGameForceHero('npc_dota_hero_techies')
	
	--set some gamerules values
	GameRules:SetCustomGameTeamMaxPlayers( DOTA_TEAM_GOODGUYS, 4)
	GameRules:SetCustomGameTeamMaxPlayers( DOTA_TEAM_BADGUYS, 0)
	GameRules:SetGoldPerTick( 0 )
	GameRules:SetHeroRespawnEnabled( false )
	GameRules:SetPreGameTime( 0 )
	GameRules:SetHeroSelectionTime( 0 )
	GameRules:SetCreepMinimapIconScale( 2.5 )
	GameRules:SetHeroMinimapIconScale( 1.5 )


end

--Initialise Pseudo Random Number Generators for drops
function BomberGameMode:InitDropRNGs()
	self.dropRNG = PseudoRNG.create( DROP_CHANCE )
	
	self.dropChoiceRNG = ChoicePseudoRNG.create({ITEM_DROP_CHANCE.speed, ITEM_DROP_CHANCE.power, ITEM_DROP_CHANCE.bombs, ITEM_DROP_CHANCE.kick})
end

--Spawn barriers according to what self.grid says
function BomberGameMode:SpawnBarriers()
	for x=0,12 do
		for y=0,10 do
			if self.grid[y][x] == 2 then
				SpawnBarrierAt(x, y)
			end
		end
	end
end

--Gets called when a hero picks up a rune
function BomberGameMode:PickupRune( hero, rune, cell )
	local runeName = rune:GetUnitName()
	local pID = hero:GetPlayerOwnerID()
		
	EmitSoundOnClient('DOTA_Item.HealingSalve.Activate', hero:GetPlayerOwner())
	
	--See what rune we have and apply its effect
	if runeName == "bomber_haste_rune" then
		--Speed is done with a modifier, check which modifier is present and update it
		--If no current speed modifier is there, apply one

		--To apply the modifier we use a datadriven item
		local item = CreateItem("item_bomber_speed_modifier", hero, hero)
		if hero:HasModifier('modifier_bomber_speed_1') then
			hero:RemoveModifierByName('modifier_bomber_speed_1')
			item:ApplyDataDrivenModifier( hero, hero, "modifier_bomber_speed_2", {})
		elseif hero:HasModifier('modifier_bomber_speed_2') then
			hero:RemoveModifierByName('modifier_bomber_speed_2')
			item:ApplyDataDrivenModifier( hero, hero, "modifier_bomber_speed_3", {})
		elseif hero:HasModifier('modifier_bomber_speed_3') then
			hero:RemoveModifierByName('modifier_bomber_speed_3')
			item:ApplyDataDrivenModifier( hero, hero, "modifier_bomber_speed_4", {})
		elseif hero:HasModifier('modifier_bomber_speed_4') then
			hero:RemoveModifierByName('modifier_bomber_speed_4')
			item:ApplyDataDrivenModifier( hero, hero, "modifier_bomber_speed_5", {})
		elseif hero:HasModifier('modifier_bomber_speed_5') then
			hero:RemoveModifierByName('modifier_bomber_speed_5')
			item:ApplyDataDrivenModifier( hero, hero, "modifier_bomber_speed_6", {})
		elseif hero:HasModifier('modifier_bomber_speed_6') then
			--return
		else
			item:ApplyDataDrivenModifier( hero, hero, "modifier_bomber_speed_1", {})
		end
		
		--show the powerup pickup on the HUD
		CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( pID ), 'bomber_player_powerup', {powerup_type = 3} )
		--log
		self.roundLog:PickupPowerup(pID, 0)
		
	elseif runeName == "bomber_power_rune" then
		--increase this player's bomb power
		self.bombPower[pID] = math.min(self.bombPower[pID]+1, 13)
		
		--show the powerup pickup on the HUD
		CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( pID ), 'bomber_player_powerup', {powerup_type = 1} )

		--log
		self.roundLog:PickupPowerup(pID, 1)
		
	elseif runeName == "bomber_bomb_rune" then
		--increase player bombs
		self.bombNum[pID] = math.min(self.bombNum[pID]+1, 10)
		
		--show the extra bomb in the UI
		--FireGameEvent("bomber_player_bombs_update", {playerID = pID, total=self.bombNum[pID], available=(self.bombNum[pID]-self.bombsPlaced[pID])})
		--FireGameEvent( "bomber_player_powerup", {playerID=pID, powerUpType=2} )
		CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( pID ), 'bomber_player_powerup', {powerup_type = 2} )

		--log
		self.roundLog:PickupPowerup(pID, 2)
		
	elseif runeName == "bomber_remote_rune" then
		--not actually used
		self.remoteBombs[pID] = true
	elseif runeName == "bomber_kick_rune" then
		--set kick variable
		self.kickBombs[pID] = true
		CustomGameEventManager:Send_ServerToPlayer( PlayerResource:GetPlayer( pID ), 'bomber_player_powerup', {powerup_type = 4} )

		--log
		self.roundLog:PickupPowerup(pID, 3)
	end

		--remove the rune
	self.grid[cell.y][cell.x] = 0
	self.drops[cell.x][cell.y] = nil
	rune:RemoveSelf()
end
-------------------------------------------------------------------------------
--Think function
-------------------------------------------------------------------------------
--Slow Think function, executed every 0.25 seconds
function BomberGameMode:Think()
	if self.gameState == GAME_STATE_BETWEEN_ROUNDS then
		--start the next round once it's time
		if GameRules:GetGameTime() > self.roundStartTime then
			self:StartRound()
			return 0.25
		end
	end
	
	if self.gameState == GAME_STATE_ROUND_END then
		--prepare the next round after the round-end screen has been displayed for a while
		if GameRules:GetGameTime() > self.nextRoundTime then
			self:PrepareNextRound()
			return 0.25
		end
	end
		
	if self.gameState == GAME_STATE_ROUND then
		self:RoundThink()
		return 0.25
	end
	
	--return how long we want to wait for another Think()
	return 0.25
end

--Fast think running at 30 calls per sec, used for all kinds of stuff
function BomberGameMode:PhysicsThink()
	local self = GameRules.bomberman

	if GameRules:State_Get() >= DOTA_GAMERULES_STATE_POST_GAME then
		-- Delete the thinker
		return
	end

	for _,hero in pairs(self.playerHeroes) do
		if hero:IsAlive() then
			-----------------------------------------
			--Do collision checking for kicking bombs
			-----------------------------------------
			local nextPos = hero:GetAbsOrigin()+hero:GetForwardVector()*(hero:GetVelocity()/30)
			local nextPosCell = WorldToGridCoords(nextPos.x, nextPos.y)
			local nextPosCenter = GridCellCenter(nextPosCell.x, nextPosCell.y, nextPos.z)
			local curPos = hero:GetAbsOrigin()
			local curCell = WorldToGridCoords(curPos.x, curPos.y)
			local pID = hero:GetPlayerOwnerID()
			
			--check if this is the last bomb this player put down
			local thisLastBomb = false
			if self.lastBomb[pID] then
				if nextPosCell.x == self.lastBomb[pID].x and nextPosCell.y == self.lastBomb[pID].y then
					thisLastBomb = true
				end
			end
					
			--if there is a bomb on this cell
			if self.grid[nextPosCell.y][nextPosCell.x] == 1 then
				--get the bomb on the cell
				local bomb = self.bombs[nextPosCell.x][nextPosCell.y]
				if bomb then
					local diffVec = vectorMinus(bomb:GetAbsOrigin(), curPos)
					
					--if we're close enough and facing the right direction we kick, otherwise block
					if vectorDist(nextPos, bomb:GetAbsOrigin()) < BOMB_RADIUS and diffVec:Dot(hero:GetForwardVector()) > 0 then
						if not thisLastBomb then
							if self.kickBombs[hero:GetPlayerOwnerID()] then
								self:KickBomb(hero, nextPosCell)
							else
								FindClearSpaceForUnit(hero, nextPosCenter - diffVec*((BOMB_RADIUS)/diffVec:Length()), false)
								hero:Hold()
							end
						end
					end
				end
			end

			if self.grid[curCell.y][curCell.x] == 3 then
				--if there is a rune on this cell and the hero is close enough, pick it up
				local rune = self.drops[curCell.x][curCell.y]
				self:PickupRune( hero, rune, curCell )
			end

			if self.grid[curCell.y][curCell.x] == 4 then
				if hero:IsAlive() then
					self:AnnounceSuicide( hero )
					hero:ForceKill( false )

					--log death
					self.roundLog:AddKill( -1, hero:GetPlayerOwnerID() )
				end
			end
		end
	end

	--Check for all moving bombs
	for _,bomb in pairs(self.movingBombs) do
		--Build in a small delay to allow for turning
		if (GameRules:GetGameTime() - bomb.moveStart) > 0.2 then
			local bombPos = bomb:GetAbsOrigin()
			local movementV = (bombPos - bomb.prevLoc)
			local removeBomb = false
			
			if movementV:Length() == 0 then
				--remove the bomb if it's not moving
				removeBomb = true
			else
				--get the current cell the bomb is in, and the cell it's moving to
				local inCell = WorldToGridCoords(bombPos.x, bombPos.y)
				local cellCenter = GridCellCenter(inCell.x, inCell.y, bombPos.z)
				local movingToCell = inCell
				local forwV = bomb:GetForwardVector()
				if math.abs(forwV.x) > math.abs(forwV.y) then
					if forwV.x > 0 then
						movingToCell.x = movingToCell.x + 1
					else
						movingToCell.x = movingToCell.x - 1
					end
				else
					if forwV.y > 0 then
						movingToCell.y = movingToCell.y + 1
					else
						movingToCell.y = movingToCell.y - 1
					end
				end

				--if the cell this bomb is moving to is not open make it stop
				if movingToCell.x < 0 or movingToCell.x > 12 or movingToCell.y < 0 or movingToCell.y > 10 or self.grid[movingToCell.y][movingToCell.x] == 1 or self.grid[movingToCell.y][movingToCell.x] == 2 or  self.grid[movingToCell.y][movingToCell.x] ==3 then
					if (cellCenter - bombPos):Dot(forwV) < 0 then
						--if the bomb is already past the center of the cell stop
						bomb:Hold()
					else
						--if the bomb is still before the center of the cell make it go to the center and stop there
						bomb:MoveToPosition(cellCenter)
					end
					
					--indicate we want to remove the bomb (happens later)
					removeBomb = true
				else
					--check the other bombs' positions, see if there is one in the cell this bomb is moving to
					local bombInNextCell = false
					for _,bomb2 in pairs(self.movingBombs) do
						local bomb2Pos = bomb2:GetAbsOrigin()
						local bomb2Cell = WorldToGridCoords(bomb2Pos.x, bomb2Pos.y, bombPos.z)
						if bomb2Cell.x == movingToCell.x and bomb2Cell.y == movingToCell.y then
							--if there is a bomb in the target cell, stop moving
							bombInNextCell = true
							break
						end
					end

					--same as for the if clause before, check where the bomb is in regard to the cell center before stopping
					if bombInNextCell then
						if (cellCenter - bombPos):Dot(forwV) < 0 then
						bomb:Hold()
						else
							bomb:MoveToPosition(cellCenter)
						end
						--remove again
						removeBomb = true
					elseif self.grid[movingToCell.y][movingToCell.x] == 0 then
						--if that's all not happening we can make the bomb go further to the next cell
						bomb:MoveToPosition(GridCellCenter(movingToCell.x, movingToCell.y, bombPos.z))
					end
				end
			end

			--If indicated we want to remove this bomb, remove it
			if removeBomb then
				local bI = -1
				for i=1,#self.movingBombs do
					if bomb == self.movingBombs[i] then
						bI = i
						break
					end
				end
				
				if bI > -1 then
					table.remove(self.movingBombs, bI)
				end
			end
		end
		--We need this to figure out the direction our bomb is travelling in
		bomb.prevLoc = bomb:GetAbsOrigin()
	end

	-- UPDATE AI STATE
	AIWorld:UpdateDangerMap(self.grid)
	
	return 0.01
end

-------------------------------------------------------------------------------
--Extra
-------------------------------------------------------------------------------
--Convert world coords into cell coordinates
function WorldToGridCoords(wx, wy)
	--blocks 224 BY 224
	local newX = math.floor((wx + 1270)/224)
	local newY = math.floor((wy + 711)/224)
	return { x = newX, y = newY }
end

--Calculate the world coords of a cell with some cell coordinates, takes an optional z value (default=0)
function GridCellCenter(cx, cy, z)
	return Vector(224 * cx - 1174, 224 * cy - 615, z or 0)
end

--The distance between two vectors
function vectorDist(v1, v2)
	return math.sqrt((v1.x-v2.x)*(v1.x-v2.x) + (v1.y-v2.y)*(v1.y - v2.y))
end

--Vector subtraction (abundant)
function vectorMinus(v1, v2)
	return Vector(v1.x-v2.x,v1.y-v2.y,v1.z-v2.z)
end

--Spawn an impenetrable barrier at a cell X and Y
function SpawnBarrierAt(x, y)
	local barrier = CreateUnitByName('bomber_barricade', GridCellCenter(x,y,GROUND_LEVEL), false, nil, nil, DOTA_TEAM_GOODGUYS)
	barrier:SetHullRadius(120)
end

--Find the newest item in a unit's inventory
function FindNewestItem( unit )
	for i=0,5 do
		if not unit:GetItemInSlot(i) then
			if i > 0 then
				return unit:GetItemInSlot(i-1)
			else
				return nil
			end
		end
	end
end

--Announce a 'killer killed victim' message in the left textbox
function BomberGameMode:AnnounceKill( killer, victim )
	local vpID = victim:GetPlayerOwnerID()
	local kpID = killer:GetPlayerOwnerID()
	--select a random kill message
	local line = KILL_LINES[math.random(1, #KILL_LINES)]
	--substitute some values in our template string
	line = line:gsub("%%vcolor", PLAYER_COLOR_BY_ID[vpID])
	line = line:gsub("%%vname", self.playerNames[vpID])
	line = line:gsub("%%kcolor", PLAYER_COLOR_BY_ID[kpID])
	line = line:gsub("%%kname", self.playerNames[kpID])
	GameRules:SendCustomMessage(line, DOTA_TEAM_GOODGUYS, 1)
end

--Announce a suicide in the left textbox
function BomberGameMode:AnnounceSuicide( victim )
	local pID = victim:GetPlayerOwnerID()
	local line = SUICIDE_LINES[math.random(1, #SUICIDE_LINES)]
	line = line:gsub("%%color", PLAYER_COLOR_BY_ID[pID])
	line = line:gsub("%%name", self.playerNames[pID])
	GameRules:SendCustomMessage(line, DOTA_TEAM_GOODGUYS, 1)
end

--Show a center message for some duration
function ShowCenterMessage( msg, dur )
	local centerMessage = {
		message = msg,
		duration = dur or 3
	}
	FireGameEvent( "show_center_message", centerMessage )
end

--Return a normalised timestamp
function GetNormTime()
	--MM/DD/YY
	date = {}
	for w in string.gmatch(GetSystemDate(),'%d+') do
		date[#date + 1] = tonumber(w)
	end

	--HH/MM/SS
	time = {}
	for w in string.gmatch(GetSystemTime(),'%d+') do
		time[#time + 1] = tonumber(w)
	end

	return time[3] + 60 * time[2] + 3600 * time[1] + 86400 * date[2] + 2592000 * date[1] + 31104000 * date[3]
end

function BomberGameMode:SpawnGridParticles()
	--Create box particle for each blocked cell
	for y,row in pairs( self.grid ) do
		for x, value in pairs( self.grid[y] ) do
			if value == -1 then
				local particle = ParticleManager:CreateParticle( 'particles/gridsquare.vpcf', PATTACH_CUSTOMORIGIN, nil )
				local center = GridCellCenter( x, y, GROUND_LEVEL )
				ParticleManager:SetParticleControl( particle, 1, center + Vector(-128, -128, 10) )
				ParticleManager:SetParticleControl( particle, 2, center + Vector(128, -128, 10) )
				ParticleManager:SetParticleControl( particle, 3, center + Vector(128, 128, 10) )
				ParticleManager:SetParticleControl( particle, 4, center + Vector(-128, 128, 10) )
			end
		end
	end

	local particle = ParticleManager:CreateParticle( 'particles/gridsquare.vpcf', PATTACH_CUSTOMORIGIN, nil )
	local center = GridCellCenter( 6, 5, GROUND_LEVEL )
	ParticleManager:SetParticleControl( particle, 1, center + Vector(-5*256-160, -4*256-160, 10) )
	ParticleManager:SetParticleControl( particle, 2, center + Vector(5*256+160, -4*256-160, 10) )
	ParticleManager:SetParticleControl( particle, 3, center + Vector(5*256+160, 4*256+160, 10) )
	ParticleManager:SetParticleControl( particle, 4, center + Vector(-5*256-160, 4*256+160, 10) )
end