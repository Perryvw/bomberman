-- Colors
PLAYER_COLOR_BY_ID = {
	[0] = "#3375FF",
	[1] = "#66FFBF",
	[2] = "#BF00BF",
	[3] = "#F3F00B",
	[4] = "#FF6B00",
	[5] = "#FE86C2",
	[6] = "#A1B447",
	[7] = "#65D9F7",
	[8] = "#008321",
	[9] = "#A46900"
}

PLAYER_COLOR_RGB = {
	[0] = {r=51, g=117, b=255},
	[1] = {r=102, g=255, b=191},
	[2] = {r=191, g=0, b=191},
	[3] = {r=243, g=240, b=11},
	[4] = {r=255, g=107, b=0},
	[5] = {r=254, g=134, b=194},
	[6] = {r=161, g=180, b=71},
	[7] = {r=101, g=217, b=247},
	[8] = {r=0, g=131, b=33},
	[9] = {r=164, g=105, b=0}
}

SUICIDE_LINES = {
	[1] = "<font color='%color'>%name</font> just killed himself!",
	[2] = "<font color='%color'>%name</font> should have watched his step."
}

KILL_LINES = {
	[1] = "<font color='%kcolor'>%kname</font> blew <font color='%vcolor'>%vname</font> sky high!",
	[2] = "<font color='%kcolor'>%kname</font> blew his load in <font color='%vcolor'>%vname</font>'s face!",
	[3] = "<font color='%kcolor'>%kname</font> just rekt <font color='%vcolor'>%vname</font>!",
	[4] = "<font color='%kcolor'>%kname</font> just 'sploded <font color='%vcolor'>%vname</font>!",
	[5] = "<font color='%kcolor'>%kname</font> blasted <font color='%vcolor'>%vname</font> into little pieces."
}