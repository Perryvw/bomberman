--------------------------------------------------------------------------------
--Script hooks for data driven abilities
--------------------------------------------------------------------------------
function placeBomb(keys)
    GameRules.bomberman:onBombPlace( keys )
end

function bombExplode(keys)
    GameRules.bomberman:onBombExplode( keys )
end