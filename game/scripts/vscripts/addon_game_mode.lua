--Libraries
require( "lib.Logging" )
require( "lib.util" )
require( "lib.PseudoRNG" )
require( "lib.Timers" )
require( "statcollection/init" )

--Core files
require( "bomberman" )
require( "events" )
require( "roundLogic" )
require( "bombLogic" )
require( "roundLog" )
require( "bombAI" )
require( "AIWorld" )

function Precache( context )
	--[[
		Precache things we know we'll use.  Possible file types include (but not limited to):
			PrecacheResource( "model", "*.vmdl", context )
			PrecacheResource( "soundfile", "*.vsndevts", context )
			PrecacheResource( "particle", "*.vpcf", context )
			PrecacheResource( "particle_folder", "particles/folder", context )
	]]
	PrecacheResource( "model", "models/props_debris/shop_set_cage001.vmdl", context )
	PrecacheResource( "particle", "particles/gridsquare.vpcf", context )
end

-- Create the game mode when we activate
function Activate()	
	GameRules.bomberman = BomberGameMode()
	GameRules.bomberman:InitGameMode()
end