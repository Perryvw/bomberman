// No spaces in event names, max length 32
// All strings are case sensitive
//
// valid data key types are:
//   string : a zero terminated string
//   bool   : unsigned int, 1 bit
//   byte   : unsigned int, 8 bit
//   short  : signed int, 16 bit
//   long   : signed int, 32 bit
//   float  : float, 32 bit
//   uint64 : unsigned int 64 bit
//   local  : any data, but not networked to clients
//
// following key names are reserved:
//   local      : if set to 1, event is not networked to clients
//   unreliable : networked, but unreliable
//   suppress   : never fire this event
//   time	: firing server time
//   eventid	: holds the event ID

"CustomEvents"
{
	"bomber_player_bombs_update"
	{
		"playerID"		"short"
		"total"			"short"
		"available"		"short"
	}
	
	"bomber_bombs_reset"
	{	
	}
	
	"bomber_start_game"
	{
	}
	
	"bomber_player_join"
	{
		"slot"			"short"
	}
	
	"bomber_point_scored"
	{
		"ID"			"short"
	}
	
	"bomber_player_powerup"
	{
		"playerID"		"short"
		"powerUpType"	"short"
	}

	"bomber_send_log_init"
	{
		"numChunks"			"short"
	}

	"bomber_send_log_chunk"
	{
		"id"				"short"
		"data"			"string"
	}

	//Stat collection
	//--------------------------------------------
	"stat_collection_part"
    {
        "data"          "string"
    }

    "stat_collection_send"
    {
    }
    //End of stat collection
    //--------------------------------------------
    //Load helper
    //--------------------------------------------
    "lh_hostid"
    {
        "hostID"       "byte"  // The ID of the player who is the host
    }
}

